function fun = create_positivity_fun(lambda)
%
% term = create_positivity_fun()
%
% Create a positivity cost function term
%
% Input:
%  data : data (optional)
% Output:
%  fun  : struct with field eval, prox and data

if nargin < 1
    lambda = 1;
end

fun.name = '+';
fun.lambda = lambda;
fun.eval = @(x) 0.5 * sum(x(x<0).^2);
fun.prox = @(x, gamma) 0.5 * (x + gamma * max(x,0))/(1 + gamma);