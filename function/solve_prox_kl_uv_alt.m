function [z1, z2] = solve_prox_kl_uv_alt(x,y,gamma)

% Calculate prox of the Kullback-Leibler divergence
% with respect to both arguments, using Newton's method.
z2 = y;

max_iter = 1000;
err = 1e-4;
zfunc = Inf;
i = 1;

while i < max_iter && norm(zfunc(:),Inf) > err
    zfunc = exp((z2-y)/gamma).*z2 - x + gamma - gamma * exp((y-z2)/gamma);
    zfuncp = exp((z2-y)/gamma).*(z2/gamma+1) + exp((y-z2)/gamma);
    z2 = z2 - zfunc./zfuncp;
    nn(i) = norm(zfunc(:),Inf);
    i = i+1;
end

if i == max_iter
   ME = MException("solve_prox_kl_ut_alt:max_itersReached",...
       "KL Prox max iterations reached, possibly not accurate!");
   throw(ME);
end

z1 = exp((z2 -y)/gamma) .* z2;
