function u = prox_convex_conj_mixed_noise(v,f,sigma,lambda1,lambda2)

% Prox of the convex conjugate of the infimal convolution 
% representing the mixed noise model by Calatroni.
% Calculated using Newton's method entry-wise to find 
% the prox (of the explicit extression of the conv conj)

debug = 0;

max_iter = 1000;
err = 1e-10;
G = Inf;
i = 1;

u = v;

uu = [];

while i < max_iter && norm(G(:)) > err
  % this shows up often in computatioins
  %w = 1-u/lambda2;
  %G = (u-v)/sigma + u/lambda1 + ...
  %  f.*((1./w-u+u/lambda2)./w.^2 ...
  %  - 1./(lambda2*w.^2) + 1./(lambda2*w));   
  %Gp = 1/sigma + 1/lambda1 + f.*((-1+1/lambda2)./w.^2 ...
  %  - 1./(lambda2*w.^4) ...
  %  + 2*(1./w-u+u/lambda2)./(lambda2*w.^3) ...
  %  - 2./(lambda2^2*w.^3) ...
  %  + 1./(lambda2^2*w.^2));

  G = (u-v)/sigma + u/lambda1 + lambda2*f./(lambda2-u);
  Gp = 1/sigma + 1/lambda1 + lambda2*f./(lambda2-u).^2;

  u = u - G./Gp;
  
  if debug
    gg(i) = norm(G(:));
  end
  
  i = i+1;
end

if debug
  semilogy(gg);
  i
  norm(G(:))
end

