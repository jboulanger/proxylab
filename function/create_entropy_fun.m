function fun = create_entropy_fun(lambda)
%
% term = create_entropy_fun(lambda)
%
% Create a cost function term of the form lambda * entropy(.) to be
% part of a problem solved by an optimization procedure
%
% Input:
%   lambda : multiplier
%   data   : data
%
% Output:
%  fun  : struct with field 'eval' and 'prox'
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

fun.lambda = lambda;
fun.eval = @(x) fun.lambda * sum( max(x(:), 0) .* log(max(x(:), 0)) );
fun.prox = @(x, gamma) fun.lambda * gamma * lambertw(exp(max(0,x) / (fun.lambda*gamma) - 1));
fun.name = 'Entropy';
