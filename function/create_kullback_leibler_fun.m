function fun = create_kullback_leibler_fun(lambda, data, alpha, background, sigma0)
%
% term = create_kullback_leibler_fun(lambda, data, alpha, background, sigma)
%
% Create a cost function term of the form lambda * DKL(.,data) to be part of a
% problem solved by an optimization procedure
%
% Input:
%  lambda : multiplier
%  data   : data
% Output:
%  fun  : struct with field 'eval' and 'prox'
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 3
    alpha = 1;
end

if nargin < 4
    background = 0;
end

if nargin < 5
    sigma0 = 0;
end

fun.data = data;
fun.lambda = lambda;
fun.alpha = alpha;

if nargin < 4
    fun.background = background;
    fun.sigma0 = sigma0;
    fun.eval = @(x) lambda * kullback_leibler_divergence(x, fun.data, fun.alpha, fun.background, fun.sigma0);
    fun.prox = @(x, gamma) prox_kullback_leibler(x, fun.data, gamma * fun.lambda, fun.alpha, fun.background, fun.sigma0);
else
    fun.background = background;
    fun.eval = @(x) lambda * kullback_leibler_divergence(x, fun.data, fun.alpha, fun.background);
    fun.prox = @(x, gamma) prox_kullback_leibler(x, fun.data, gamma * fun.lambda, fun.alpha, fun.background);
end

fun.name = 'Kullback Leibler Divergence';
