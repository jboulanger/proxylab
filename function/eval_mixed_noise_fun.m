function [Phi, v] = eval_mixed_noise_fun(u,f,lambda1,lambda2);

% Evaluate the infimal convolution representing the mixed noise 
% model by Calatroni
%
% Input:  
%  f : data
%  u : iterand
% Output:
%  Phi : the value of the infimal convolution
%  v : arginf_v { ... }

% Entrywise, Newton's method on the derivative of the expression
% inside the inf, then evaluate the expression at that value.

debug = 0;

if ndims(u) == 2
  max_iter = 1000;
  err = 1e-12;
else
  err = 1e-8;
  max_iter = 1000;
end
F = Inf;
i = 1;

v = zeros(size(f));


while i < max_iter && norm(F(:)) > err
  F = lambda1*v + lambda2*(1 - f./(f-v) - log((f-v)./u) + v./(f-v));
  Fp = lambda1 + lambda2./(f-v);

  v = v - F./Fp;

  if debug
    ff(i) = norm(F(:));
  end
  
  i = i+1;
end

Phi = lambda1/2 * norm(v(:),2).^2 + lambda2*kullback_leibler_divergence_mixed(u,f-v);

if debug
  figure(2)
  semilogy(ff);
  ff(end)
  i
end


