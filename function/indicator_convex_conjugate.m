function g = indicator_convex_conjugate(y, bounds)
% The convex conjugate of the set defined by the bounds by
% bounds(1) \leq y_i \leq bounds(2), \forwall i

    y = y(:);
    g = sum(y(y > 0) * bounds(2)) + sum(y(y<0) * bounds(1));
end
