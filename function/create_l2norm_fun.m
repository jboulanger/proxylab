function fun = create_l2norm_fun(lambda, data, weights)
%
% term = create_l2norm_fun(lambda, data, weights)
%
% Create a cost function term of the form lambda * |.-data|^2/2 to be part of a
% problem solved by an optimization procedure
%
% Input:
%  lambda    : multiplier
%  data      : data (optional)
%  weights   : weights (optional)
% Output:
%  fun  : struct with field 'eval' and 'prox'
%
%  fun.eval = @(x) lambda norm( x - data )^2 /2
%  fun.prox = @(x,gamma) (x + lamdba * gamma * data) / (1 + lambda * gamma)
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 1
    lambda = 1;
end

fun.lambda = lambda;
fun.name = 'l2 norm';
if nargin < 3
    if nargin > 1
        fun.data = data;
        fun.eval = @(x) fun.lambda .* norm(x(:) - fun.data(:),2)^2 / 2;
        fun.prox = @(x, gamma) (x + gamma .* fun.lambda * fun.data) ./ (1 + gamma * fun.lambda);
        fun.conv_conj = @(y) fun.lambda * convex_conjugate(y / fun.lambda, fun.data);
    else
        fun.eval = @(x) fun.lambda .* norm(x(:),2)^2 / 2;
        fun.prox = @(x, gamma) x ./ (1 + gamma * fun.lambda);
        fun.conv_conj = @(y) fun.lambda * convex_conjugate(y / fun.lambda, 0);
    end
else
    fun.data = data;
    fun.weights = weights;
    fun.eval = @(x) fun.lambda .* norm(1./sqrt(fun.weights(:)) .* (x(:) - fun.data(:)),2)^2 / 2;
    fun.prox = @(x, gamma) (x + gamma * fun.lambda .* fun.weights  .* fun.data) ./ (1 + gamma * fun.lambda * fun.weights);
end
end

function c = convex_conjugate(y, data)
    y = y(:);
    data = data(:);
    c = (y + data)' * y - 1/2 * norm(y,2)^2;
end

