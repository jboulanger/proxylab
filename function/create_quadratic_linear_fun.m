function fun = create_quadratic_linear_fun(lambda, A, data, domain, is_otf, dim)
%
% term = CREATE_QUADRATIC_LINEAR_FUN(lambda, A, data, domain, is_otf, dim);
%
% Create a cost function term of the form lambda/2 * |Ax - data|^2 to be part of a
% problem solved by an optimization procedure.
%
% A can represent either a matrix multiplication or a fft OTF/PSF for
% convolution.
%
%
% Input:
%  lambda : multiplier
%  A      : matrix or OTF / PSF
%  domain : 'direct' / 'spectral'
%  is_otf : is domain is 'spectral' tells if A is the OTF instead of the
%           PSF
%  dim    : dimensions to compute the OTF from A
%  data   : data (optional)
%
% Output:
%  fun  : struct with field 'name', 'eval', 'grad' and 'prox'
%
% See also: ppxa
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 1
    lambda = 1;
end

if nargin < 4
    domain = 'direct';
end

fun.lambda = lambda;
fun.data = data;
fun.name = 'quadratic linear';
if ~strcmp(domain,'spectral')
    fun.A = A;
    fun.AtA = A' * A;
    fun.Aty = A' * fun.data;
    fun.I =  eye(size(fun.AtA));
    fun.eval = @(x) norm( A * x - fun.data )^2;
    fun.prox = @(x,gamma) (fun.I + fun.lambda * gamma * fun.AtA) \ (x + fun.lambda * gamma * fun.Aty);
    fun.L = max(svd(A'*A));
    fun.grad = @(x) (fun.AtA * x - fun.Aty);
else
    if is_otf == true
        fun.A = A;
    else
        fun.A = psf2otf(A, dim);
    end
    fun.Aty = conj(A) .* fftn(data);
    fun.AtA = conj(A) .* A;
    fun.eval = @(x) norm( real(ifftn(fftn(x) .* fun.A)) * x - fun.data )^2;
    fun.prox = @(x,gamma) real(ifftn((fftn(x) + fun.lambda * gamma * fun.Aty) ./ (1 + fun.lambda * fun.AtA)));
    fun.grad = @(x) real(ifftn(fftn(x) .* fun.AtA)) - fun.Aty;
    fun.L = max(A);
end
