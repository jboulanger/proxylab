function val = schatten_norm(x,p)
%
% y = schatten_norm(x)
%
% Computes the Schatten norm (p=1) of x.
%
% Input :
%  x : either a cell representing a tensor or a matrix field as an array
%  p : type of the norm
% Output:
%  val : schatten norm of x
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 2
    p = 1;
end

val = 0;

if ndims(x) == 3 && size(x,3) == 3; % tensor 2x2
    for k = 1:size(x,1)
        for l = 1:size(x,2)
            val = val + sum(norm(svd([x(k,l,1) x(k,l,2) ; x(k,l,2) x(k,l,3)]),p));
        end
    end
elseif ndims(x) == 4 % field of matrices
    for k = 1:size(x,1)
        for l = 1:size(x,2)
            val = val + sum(norm(svd(reshape(x(k,l,:,:), size(x,3), size(x,4))),p));
        end
    end
end
