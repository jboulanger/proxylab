function fun = create_quadratic_op_fun(lambda, operator, data)
%
% term = create_quadratic_linear_fun(lambda, operator, data);
%
% Create a cost function term of the form lambda/2 * |Ax - data|^2 to be part of a
% problem solved by an optimization procedure.
%
% A can represent either a matrix multiplication or a fft OTF/PSF for
% convolution.
%
% Input:
%  lambda : multiplier
%  A      : matrix or OTF / PSF
%  domain : 'direct' / 'spectral'
%  is_otf : is domain is 'spectral' tells if A is the OTF instead of the
%           PSF
%  dim    : dimensions to compute the OTF from A
%  data   : data (optional)
%
% Output:
%  fun  : struct with field 'eval', 'grad' and 'prox'
%
% Nelly Pustelnik  (nelly.pustelnik@ens-lyon.fr)
% Laurent Condat   (laurent.condat@gipsa-lab.grenoble-inp.fr)
% Jerome Boulanger (jerome.boulanger@curie.fr)

if nargin < 1
    lambda = 1;
end

fun.name = 'quadratic A';
fun.lambda = lambda;
fun.data = data;
fun.operator = operator;
fun.Aty = fun.operator.apply_adjoint(data);
fun.eval = @(x) 0.5 * norm( fun.operator.apply(x) - fun.data )^2;
fun.prox = @(x,gamma) fun.operator.resolvant(x + gamma * fun.lambda * fun.Aty, gamma * fun.lambda);
fun.grad = @(x) 0.5 * fun.operator.apply_adjoint(fun.operator.apply(x) - fun.data);
% Lipschitz constant defined by |\nabla f(x)- \nabla f(y)| < L |x - y|
% If f(x) = |Ax-b|²/2 then L = max(svd(A'A))
fun.L = fun.operator.norm;