function z = prox_kullback_leibler_uv(x, gamma, idx, g, m, sigma)
% PROX_KULLBACK_LEIBLER_UV Apply proximal operator of the Kullback Leibler divergence
%
% z = PROX_KULLBACK_LEIBLER_UV(x, gamma, idx, a, b, sigma)
%
% Compute the proximal map of the Kullback Leilbler divergence
%  prox_{gamma KL}(z) = 

% assuming that we have y = a x + z with x/a Poisson and z ~ N(b,sigma^2)
%
% Scaling (a) and translation (b) are based on : P. L. Combettes and J.-C.
% Pesquet, “Proximal Splitting Methods in Signal Processing,” in
% Fixed-Point Algorithms for Inverse Problems in Science and Engineering,
% vol. 49, H. H. Bauschke, R. S. Burachik, P. L. Combettes, V. Elser, D. R.
% Luke, and H. Wolkowicz, Eds. New York, NY: Springer New York, 2011, pp.
% 185–212.
%

if nargin < 3
    idx = 0;
end
if nargin < 4
    g = 1;
end
if nargin < 5
    m = 0;
end
if nargin < 6
    sigma = 0;
end


if ndims(x) == 3
    if sum(idx) == 0
        yp = x(:,:,2);
        xp = x(:,:,1);
    else
        yp = x(idx,:,2);
        xp = x(idx,:,1); 
    end
elseif ndims(x) == 4
    if sum(idx) == 0
        yp = x(:,:,:,2);
        xp = x(:,:,:,1);
    else
        yp = x(:,:,idx,2);
        xp = x(:,:,idx,1);
    end
else
    error('Not implemented')
end

if sum(idx) == 0
    z = zeros(size(x),'like',x);
else
    z = zeros([size(xp),2],'like',x);
end

if g==1 && m == 0 && sigma == 0
    [z1, z2] = solve_prox_kl_uv_alt(xp,yp,gamma); 
    if ndims(z) == 3
      z(:,:,1) = z1;
      z(:,:,2) = z2;
    elseif ndims(z) == 4
      z(:,:,:,1) = z1;
      z(:,:,:,2) = z2;
    end
else
    a = g;
    if nargin < 6
        b = - m / g;
    else
        b = m - sigma^2/g;
    end
    x = (x - b) / a;
    yp = (yp - b) / a;

    [z1, z2] = solve_prox_kl_uv_alt(xp,yp,gamma); 
    if ndims(z) == 3
      z(:,:,1) = z1;
      z(:,:,2) = z2;
    elseif ndims(z) == 4
      z(:,:,:,1) = z1;
      z(:,:,:,2) = z2;
    end    
    z = a * z + b;
end
