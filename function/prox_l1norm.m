function y = prox_l1norm(x, gamma, is_vectorial)
%
% y = prox_l1norm(x, gamma, D)
%
% Compute  argmin_x |x-y|^2 + gamma |x|_1
%
% If x is an array then computes the soft thresholding of x
% If x is a cell then compute the vectorial version taking the norm
% D is the dimension of the array x (2D or 3D)
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if isreal(x)
    if ~is_vectorial
        y = max( 0, x - gamma ) - max( 0, -x - gamma );
    else
        D = ndims(x) - 1;
        if D == 2
            N = sqrt( sum(x.^2, 3) );
            N = cat(3,N,N);
        elseif D == 3
            N = sqrt( sum(x.^2, 4) );
            N = cat(4,N,N,N);
        else
            error('Undefined number of dimensions (%d).', D);
        end
        I = N > gamma;
        y = zeros(size(x),class(x));
        y(I) = (1 - gamma ./ N(I)) .* x(I);
    end
else
    N = abs(x);
    I = N > gamma;
    y = zeros(size(x),class(x));

    y(I) = (1 - gamma ./ N(I)) .* x(I);
end
