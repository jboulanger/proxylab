function fun = create_weighted_l2norm_fun(lambda, data)
%
% term = create_weighted_l2norm_fun(data)
%
% Create a cost function term of the form lambda * W|.-data|^2/2 to be part of a
% problem solved by an optimization procedure
%
% Input:
%  lambda : multiplier
%  data   : data (optional)
% Output:
%  fun  : struct with field 'eval' and 'prox'
%
%  fun.eval = @(x) lambda norm( x - data )^2 /2
%  fun.prox = @(x,gamma) (x + lamdba * gamma * data) / (1 + lambda * gamma)
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 1
    lambda = 1;
end

fun.lambda = lambda;
fun.name = 'l2 norm';

if nargin > 1
  fun.data = data;
  fun.eval = @(x) fun.lambda * norm(x(:) - fun.data(:))^2 / 2;
  fun.prox = @(x, gamma) (x + gamma * fun.lambda * fun.data) / (1 + gamma * fun.lambda);
else
  fun.eval = @(x) fun.lambda * norm(x(:))^2 / 2;
  fun.prox = @(x, gamma) x / (1 + gamma * fun.lambda);
end
