function fun = create_l1norm_fun(lambda, data, is_vectorial)
%
% term = create_l1norm_fun(lambda,data,is_vectorial)
%
% Create a cost function term of the form |.-b|^2 to be part of a
% problem solved by an optimization procedure
%
% Input:
%  lambda : multiplier
%  data : data (optional)
% Output:
%  fun  : struct with field eval, prox and data
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 1
    lambda = 1;
end

if nargin < 2
    data = [];
end

if nargin < 3
    is_vectorial = false;
end

fun.lambda = lambda;
fun.name = 'l1 norm';
fun.is_vectorial = is_vectorial;

if ~isempty(data)
    fun.data = data;
    fun.eval = @(x) fun.lambda * norm(x(:) - fun.data(:), 1);
    fun.prox = @(x, gamma) fun.data + prox_l1norm(x - fun.data, fun.lambda * gamma, fun.is_vectorial);
else
    fun.eval = @(x) fun.lambda * norm(x(:), 1);
    fun.prox = @(x, gamma) prox_l1norm(x, fun.lambda * gamma, fun.is_vectorial);
    fun.conv_conj = @(y) fun.lambda * convex_conjugate(y / fun.lambda);
end
end

function c = convex_conjugate(y, data)
    n = norm(y(:),Inf);
    if n > 1+1e-6
        c = Inf;
    else
        c = 0;
    end
    
    if nargin > 1
        c = c + y(:)'*data(:);
    end
end

