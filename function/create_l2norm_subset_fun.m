function fun = create_l2norm_subset_fun(idx, lambda, data)
%
% term = create_l2norm_fun(lambda, data, weights)
%
% Create a cost function term of the form lambda * |.-data|^2/2 to be part of a
% problem solved by an optimization procedure.
%
% Only apply to z-slices corresponding to indices in idx
%
% Input:
%  lambda    : multiplier
%  data      : data (optional)
% Output:
%  fun  : struct with field 'eval' and 'prox'
%
%  fun.eval = @(x) lambda norm( x - data )^2 /2
%  fun.prox = @(x,gamma) (x + lamdba * gamma * data) / (1 + lambda * gamma)
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)


if nargin < 2
    lambda = 1;
end

if nargin < 3
    data = 0;
end

fun.lambda = lambda;
fun.data = data;
fun.idx = idx;
fun.name = 'l2 norm';
fun.eval = @(x) fun.lambda .* l2_eval(x-fun.data, fun.idx);
fun.prox = @(x, gamma) l2_prox(x, gamma, lambda, data, idx);
fun.conv_conj = @(y) fun.lambda * l2_convex_conjugate(y / fun.lambda, fun.data, fun.idx);

end

function c = l2_eval(x, idx)
    if ndims(x) == 2
        x = x(idx,:);
    elseif ndims(x) == 3
        x = x(:,:,idx);
    else
        error('Not implemented');
    end

    c = norm(x(:),2)^2 / 2;
end

function c = l2_prox(x, gamma, lambda, data, idx)
    if ndims(x) == 2
        x = x(idx,:);
        data = data(idx,:);
    elseif ndims(x) == 3
        x = x(:,:,idx);
        data = data(:,:,idx);
    else
        error('Not implemented');
    end

    c = (x + gamma .* lambda * data) ./ (1 + gamma * lambda);
end

function c = l2_convex_conjugate(y, data, idx)
    if ndims(y) == 2
        y = y(idx,:);
        data = data(idx,:);
    elseif ndims(y) == 3
        y = y(:,:,idx);
        data = data(:,:,idx);
    else
        error('Not implemented');
    end
    
    y = y(:);
    data = data(:);
    c = (y + data)' * y - 1/2 * norm(y,2)^2;
end

