function fun = create_schatten_norm_fun(lambda, p, data)
%
% term = CREATE_SHATTEN_NORM_FUN(lambda,p,data)
%
% Create a cost function term of the form |.-b|^2 to be part of a
% problem solved by an optimization procedure
%
% Input:
%  data : data (optional)
% Output:
%  fun  : struct with field eval, prox and data
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

fun.name = 'Sp';

if nargin < 2
   p = 1;
end

fun.lambda = lambda;
fun.p = p;

if nargin > 2
  fun.data = data;
  fun.eval = @(x) fun.lambda * schatten_norm(x - fun.data, fun.p);
  fun.prox = @(x, y) fun.data + prox_schatten_norm(x - fun.data, fun.lambda * y, fun.p);
else
  fun.eval = @(x) fun.lambda * schatten_norm(x, fun.p);
  fun.prox = @(x, y) prox_schatten_norm(x, fun.lambda * y, fun.p);
end
