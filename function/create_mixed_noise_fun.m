function fun = create_mixed_noise_fun(lambda1,lambda2,data)
%
% Input:
%  lambda1 : coefficient for l2 fidelity 
%  lambda2 : coefficient for Kullback-Leibler Divergence fidelity
%  data   : data
% Output:
%  fun  : struct with field 'eval' and 'prox'


fun.data = data;
fun.lambda1 = lambda1;
fun.lambda2 = lambda2;

fun.eval = @(x) eval_mixed_noise_fun(x,fun.data,fun.lambda1,fun.lambda2);
fun.prox_conj = @(x,gamma) prox_convex_conj_mixed_noise(x,fun.data,gamma,fun.lambda1,fun.lambda2);

fun.name = 'Mixed Noise Fun';
