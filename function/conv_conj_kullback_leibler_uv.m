function s = conv_conj_kullback_leibler_uv(xs,bounds,idx)
% Calculate the convex conjugate of the Kullback-Leibler divergence
% w.r.t. both arguments, over the set determined by "bounds"

if nargin < 3
    idx = 0;
end

l1 = bounds(1);
l2 = bounds(2);

if ndims(xs) == 3
    if sum(idx) == 0
        fs = xs(:,:,2);
        us = xs(:,:,1);
    else
        fs = xs(idx,:,2);
        us = xs(idx,:,1);
    end
elseif ndims(xs) == 4
    if sum(idx) == 0
        fs = xs(:,:,:,2);
        us = xs(:,:,:,1);
    else
        fs = xs(:,:,idx,2);
        us = xs(:,:,idx,1);
    end
end

fs = fs(:);
us = us(:);

if isa(xs,'gpuArray')
    minusInf = gpuArray(-Inf);
else
    minusInf = -Inf;
end

v = zeros(length(fs), 9, 'like', xs);

% The sup is attained in the interior of the domain
vv = abs(fs - log(complex(1-us)));
ix = vv < eps;
vv(:) = minusInf;
vv(ix & us < 1) = 0;
v(:,1) = vv;

u = l2;
f = l1;
vv = u.*us + f.*fs - u + f - f.*log(f) + f.*log(u);
% Then make sure the corresponding Lagrange multipliers are
% not negative at this point
mu1 = us - 1 + l1/l2;
mu4 = -fs + log(l1/l2);
vv(mu1 < 0 |  mu4 < 0) = minusInf;
v(:,2) = vv;

u = l2;
f = l2.*exp(fs);
vv = u.*us + f.*fs - u + f - f.*log(f) + f.*log(u);
mu1 = us + exp(fs) - 1;
vv(mu1 < 0) = minusInf;
v(:,3) = vv;

u = l2;
f = l2;
vv = u.*us + f.*fs - u + f - f.*log(f) + f.*log(u);
mu1 = us;
mu2 = fs;
vv(mu1 < 0 | mu2 < 0) =  minusInf;
v(:,4) = vv;

u = complex(l2./(1-us));
f = l2;
vv = u.*us + f.*fs - u + f - f.*log(f) + f.*log(u);
mu2 = fs - log(complex(1-us));
vv(mu2 < 0) = minusInf;
vv(~isreal(mu2)) = minusInf;
vv(us > 1) = minusInf;
v(:,5) = vv;

u = l1;
f = l2;
vv = u.*us + f.*fs - u + f - f.*log(f) + f.*log(u);
mu3 = -us + 1 - l2/l1;
mu2 = fs - log(l2) + log(l1);
vv(mu3 < 0 | mu2 < 0) =  minusInf;
v(:,6) = vv;

u = l1;
f = l1 .* exp(fs);
vv = u.*us + f.*fs - u + f - f.*log(f) + f.*log(u);
mu3 = 1 - us - exp(fs);
vv(mu3 < 0) =  minusInf;
v(:,7) = vv;

u = l1;
f = l1;
vv = u.*us + f.*fs - u + f - f.*log(f) + f.*log(u);
mu3 = -us;
mu4 = -fs;
vv(mu3 < 0 | mu4 < 0) =  minusInf;
v(:,8) = vv;

u = complex(l1./(1-us));
f = l1;
vv = u.*us + f.*fs - u + f - f.*log(f) + f.*log(u);
mu4 = -fs + log(complex(1-us));
vv(mu4 < 0) =  minusInf;
vv(~isreal(mu4)) =  minusInf;
vv(us > 1) = minusInf;
v(:,9) = vv;

imagv = imag(v);
if sum(abs(imagv(:))) > 0
   ME = MException("conv_conj_kullback_leibler_uv:non_real",...
       "Some entries in v are imaginary, something has gone wrong!");
   throw(ME);
end

s = sum(max(real(v),[],2));




