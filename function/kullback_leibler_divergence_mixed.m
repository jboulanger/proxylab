function d = kullback_leibler_divergence_mixed(x, y, a, b, sigma)
%
%  d = kullbackleibler_divergence(x,y,a,b)
%
%  Compute the Kullback Leibler divergence as given in eq. (2.3) in [1].
%
%  is a ~= 1 or b~= 0 the data is transform before hand as (y-b)/a
%
%  Input
%   x : input array
%   y : input array
%   a : scaling (scalar)
%   b : translation (scalar)
%
% Output
%   d : the divergence (scalar)
%
% Note: if y is empty, used the last dimension of x to extract x and y
%
% [1] L. Calatroni, J.C. Reyes, C.-B. Schonlieb, "Infimal convolution
% of data discrepancies for mixed noise removal"
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 3
    a = 1;
end
if nargin < 4
    b = 0;
end
if nargin < 5
    sigma = 0;
end

x = a * x + sigma - a * b;
y = a * y + sigma - a * b;

idx1 = x > 0 & y > 0;
idx2 = x > 0 & y == 0;
%d = sum(x(idx1(:)) - y(idx1(:)) .* log(x(idx1(:)))) + sum(x(idx2(:)));
%d = sum(x(idx1(:)) - y(idx1(:)) .* log(x(idx1(:))));

d = sum(x(idx1(:)) - y(idx1(:)) + y(idx1(:)) .* log(y(idx1(:))) - y(idx1(:)) .* log(x(idx1(:)))); 
