function fun = create_kullback_leibler_uv_fun(lambda, bounds, idx, alpha, background, sigma0)
%
% term = create_kullback_leibler_fun(lambda, data, alpha, background, sigma)
%
% Create a cost function term of the form lambda * DKL(.,data) to be part of a
% problem solved by an optimization procedure
%
% Input:
%  lambda : multiplier
%  idx : only work with the z-slices corresponding to indices in idx
%  data   : data
% Output:
%  fun  : struct with field 'eval' and 'prox'
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 2
    bounds = [-Inf, Inf];
end

if nargin < 3
    idx = 0;
end

if nargin < 4
    alpha = 1;
end

if nargin < 5
    background = 0;
end

if nargin < 6
    sigma0 = 0;
end

fun.lambda = lambda;
fun.bounds = bounds;
fun.idx = idx;
fun.alpha = alpha;

if nargin < 5
    fun.background = background;
    fun.sigma0 = sigma0;
    fun.eval = @(x) lambda * kullback_leibler_divergence_uv(x, fun.idx, fun.alpha, fun.background, fun.sigma0);
    fun.prox = @(x, gamma) prox_kullback_leibler_uv(x, gamma * fun.lambda, fun.idx, fun.alpha, fun.background, fun.sigma0);
    fun.conv_conj = @(xs) fun.lambda * conv_conj_kullback_leibler_uv(xs/fun.lambda, fun.bounds, fun.idx);
else
    fun.background = background;
    fun.eval = @(x) lambda * kullback_leibler_divergence_uv(x, fun.idx, fun.alpha, fun.background);
    fun.prox = @(x, gamma) prox_kullback_leibler_uv(x, gamma * fun.lambda, fun.idx, fun.alpha, fun.background);
end

fun.name = 'Kullback Leibler Divergence UV';
