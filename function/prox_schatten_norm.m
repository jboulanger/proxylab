function y =  prox_schatten_norm(x, gamma, p)
%
% y = prox_schatten_norm(x, gamma)
%
% Computes the proximal operator associated to the Schatten norm (p=1).
%
% Input
%  x     : either a field of tensors or a matrix field as an array
%  gamma : multiplier
%
% Output
%  y     : evaluation of the proximity operator at point x
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

y = zeros(size(x),class(x));

if ndims(x) == 3 && size(x,3) == 4 % tensor 2x2
    if 0 % non optimized reference code
        for k = 1:size(x,1)
            for l = 1:size(x,2)
                H = prox_sp([x(k,l,1), x(k,l,2) ; x(k,l,3), x(k,l,4)], gamma, p);
                y(k,l,1) = H(1,1);
                y(k,l,2) = H(1,2);
                y(k,l,3) = H(2,1);
                y(k,l,4) = H(2,2);
            end
        end
    else
        a = x(:,:,1);
        b = x(:,:,2);
        c = x(:,:,3);
        d = x(:,:,4);

        e = a + d;
        f = e .* e - 4 * (a .* d - b .* c);
        f = sqrt(abs(f));

        l1 = 0.5 * (e - f);
        l2 = 0.5 * (e + f);

        theta1 = atan2(l2 - a, b);
        theta2 = atan2(l1 - a, b);

        u1 = cos(theta1);
        u2 = cos(theta2);
        u3 = sin(theta1);
        u4 = sin(theta2);


        l1 = max( 0, l1 - gamma ) - max( 0, -l1 - gamma );
        l2 = max( 0, l2 - gamma ) - max( 0, -l2 - gamma );

        v1 = u1 .* l2;
        v2 = u2 .* l1;
        v3 = u3 .* l2;
        v4 = u4 .* l1;

        y(:,:,1) = u1 .* v1 + u2 .* v2;
        y(:,:,3) = u1 .* v3 + u2 .* v4;
        y(:,:,2) = u3 .* v1 + u4 .* v2;
        y(:,:,4) = u3 .* v3 + u4 .* v4;

    end
elseif ndims(x) == 4 % field of matrices
    if (exist('cimgprox_schatten', 'file') == 3)
        % reshape x as the mex function take field of matrix as vectors
        tmp = reshape(x, size(x,1), size(x,2), size(x,3) * size(x,4));
        % apply the proximal operator
        y = cimgprox_schatten(tmp, size(x,3), size(x,4), gamma);
        % reshape y as x
        y = reshape(y, size(x));
    else
        for k = 1:size(x,1)
            for l = 1:size(x,2)
                y(k,l,:,:) = prox_sp(reshape(x(k,l,:,:), size(x,3), size(x,4)), gamma, p);
            end
        end
    end
end

function y = prox_sp(x, gamma, p)
[U,S,V] = svd(x);
if p == 1
    S = max( 0, S - gamma ) - max( 0, -S - gamma );
elseif p ==2
    S = S / (1 + gamma);
end
y = U * S * V';
