function z = prox_kullback_leibler(x, y, gamma, g, m, sigma)
% PROX_KULLBACK_LEIBLER Apply proximal operator of the Kullback Leibler divergence
%
% z = PROX_KULLBACK_LEIBLER(x, y, gamma, a, b, sigma)
%
% Compute the proximal map of the Kullback Leilbler divergence
%  prox_{gamma KL}(z) = (y - gamma + sqrt((y - gamma)^2 + 4 gamma y) / 2
%
% assuming that we have y = a x + z with x/a Poisson and z ~ N(b,sigma^2)
%
% Note: if y is empty, used the last dimension of x to extract x and y
%
% Scaling (a) and translation (b) are based on : P. L. Combettes and J.-C.
% Pesquet, “Proximal Splitting Methods in Signal Processing,” in
% Fixed-Point Algorithms for Inverse Problems in Science and Engineering,
% vol. 49, H. H. Bauschke, R. S. Burachik, P. L. Combettes, V. Elser, D. R.
% Luke, and H. Wolkowicz, Eds. New York, NY: Springer New York, 2011, pp.
% 185–212.
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 4
    g = 1;
end
if nargin < 5
    m = 0;
end
if nargin < 6
    sigma = 0;
end
if isempty(y)
    if ndims(x) == 3
        y = x(:,:,2);
        x = x(:,:,1);
    elseif ndims(x) == 4
        y = x(:,:,:,2);
        x = x(:,:,:,1);
    else
        error('Not implemented')
    end    
end

if g==1 && m == 0 && sigma == 0
    z = (x - gamma + sqrt(max(0, (x - gamma).^2 + 4 * gamma * y))) / 2;
else
    a = g;
    if nargin < 6
        b = - m / g;
    else
        b = m - sigma^2/g;
    end
    x = (x - b) / a;
    y = (y - b) / a;
    z = (x - gamma + sqrt(max(0,(x - gamma).^2 + 4 * gamma * y))) / 2;
    z = a * z + b;
end
