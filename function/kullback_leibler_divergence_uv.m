function d = kullback_leibler_divergence_uv(x, idx, a, b, sigma)
%
%  d = kullbackleibler_divergence(x,y,a,b)
%
%  Compute the Kullback Leibler divergence
%
%  d = sum_k  { x_k - y_k log(x_k)if (x_k > 0 & y_k >0); x_k if x_k > 0 }
%
%  is a ~= 1 or b~= 0 the data is transform before hand as (y-b)/a
%
%  Input
%   x : input array
%   y : input array
%   a : scaling (scalar)
%   b : translation (scalar)
%
%
% Output
%   d : the divergence (scalar)
%
% Note: if y is empty, used the last dimension of x to extract x and y
%
% A. Chakrabarti and T. Zickler, “Image Restoration with Signal-dependent
% Camera Noise,” arXiv:1204.2994 [cs, stat], Apr. 2012.
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 2
    idx = 0;
end
if nargin < 3
    a = 1;
end
if nargin < 4
    b = 0;
end
if nargin < 5
    sigma = 0;
end
if ndims(x) == 3
    if sum(idx) == 0
        y = x(:,:,2);
        x = x(:,:,1);
    else
        y = x(idx,:,2);
        x = x(idx,:,1);
    end
elseif ndims(x) == 4
    if sum(idx) == 0
        y = x(:,:,idx,2);
        x = x(:,:,idx,1);
    else
        y = x(:,:,idx,2);
        x = x(:,:,idx,1);
    end
else
    error('Not implemented')
end


x = a * x + sigma - a * b;
y = a * y + sigma - a * b;

idx1 = x > 0 & y > 0;
idx2 = x > 0 & y == 0;
%d = sum(x(idx1(:)) - y(idx1(:)) .* log(x(idx1(:)))) + sum(x(idx2(:)));
%d = sum(x(idx1(:)) - y(idx1(:)) .* log(x(idx1(:))));

d = sum(x(idx1(:)) - y(idx1(:)) + y(idx1(:)) .* log(y(idx1(:))) - y(idx1(:)) .* log(x(idx1(:)))); 
