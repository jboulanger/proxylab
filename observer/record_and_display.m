function output = record_and_display(cost, options, output, x,  k, t0, primal_cost, pd_gap)
% 
% Input:
%  cost : cost function
%  options: options
%  output: previous output
%  x : current estimate
%  k : current iteration
%  t0 : initial time
%  primal : primal cost
%  dual :dual cost
%

output.estimate = x;

if ~options.record
    output.iteration = k;
    output.elapsed_time = toc(t0);
else
    if isfield(output, 'iteration') && isfield(output, 'cost')
        idx = size(output.cost,1) + 1;
    else
        idx = 1;
    end
    
    output.iteration(idx) = k;
    output.elapsed_time(idx) = toc(t0);
        
    if nargin > 6
        for i = 1:numel(primal_cost)
            output.cost(idx,i) = to_cpu(primal_cost(i));
        end
    end
    if nargin == 8
        output.pd_gap(idx) = to_cpu(pd_gap);
    end
end

if isfield(options, 'observer')
    options.observer(cost, options, output);
end
