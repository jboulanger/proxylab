function progressbar(cost, options, output)
%
% progressbar(cost, options, output)
%
% Print and display information on minimization algorithm as a text
% progress bar
%
% Input:
%   cost    : is an array struct representing a cost function sum_k f_k(T_kx)
%   options : is a struct with options (algorithm, cost)
%   output  : is the current output of the minimization algorithm
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

persistent count;
k = output.iteration(end);
n = options.max_iter;
if mod(k,10) == 0 || k == 1 || k == n
    if k == 1
         fprintf('\n');
    elseif ~isempty(count)
        for i = 1:count
            fprintf(1,'\b');
        %fprintf('1')
        end
    end
    txt = sprintf('<<~~ %s [%3.0f%%] %d/%d ~~', options.algorithm, 100*k/n,k,n);
    count = length(txt);
    fprintf(1,'%s',txt);
    if k == n
        fprintf('>>\n');
    end
end
