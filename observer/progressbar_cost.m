function progressbar_cost(cost, options, output)
%
% progressbar(cost, options, output)
%
% Print and display information on minimization algorithm as a text
% progress bar
%
% Input:
%   cost    : is an array struct representing a cost function sum_k f_k(T_kx)
%   options : is a struct with options (algorithm, cost)
%   output  : is the current output of the minimization algorithm
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

persistent count;
k = output.iteration(end);
n = options.max_iter;
if mod(k,10) == 0 || k == 1 || k == n
    if k == 1
         fprintf('\n');
    elseif ~isempty(count)
        for i = 1:count
            fprintf(1,'\b');
        %fprintf('1')
        end
    end
    if ~isfield(output,'cost')
        txt = sprintf('<<~~ %s [%3.0f%%] %d/%d ~~', ...
            options.algorithm, 100*k/n,k,n);  
    else
        c = sum(output.cost(end,:));
        if ~isfield(output,'pd_gap')
            txt = sprintf('<<~~ %s [%3.0f%%] %d/%d, cost=%0.2e ~~', ...
                options.algorithm, 100*k/n,k,n,c);
        else
            % Note that the primal-dual gap is not normalised by the range 
            % of the data (which is what the tolerance uses)
            g = output.pd_gap(end)/numel(output.estimate);
            txt = sprintf('<<~~ %s [%3.0f%%] %d/%d, cost=%0.2e, pd_gap=%0.2e ~~', ...
                options.algorithm, 100*k/n,k,n,c,g);
        end
        
    end
    count = length(txt);
    fprintf(1,'%s',txt);
    if k == n
        fprintf('>>\n');
    end
end
