function operator = create_stacked_op(O)
%
% operator = create_composed_op(O)
%
% Create an operator from a list of operators
%
%

operator.name = 'stacked';
operator.list = O;
operator.apply = @(x) apply_stacked(operator.list, x);
operator.apply_adjoint = @(x) apply_adjoint_stacked(operator.list, x);
operator.norm = numel(O);
