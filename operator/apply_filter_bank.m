function result = apply_filter_bank(filter_bank, data, domain)
%
% result = apply_filter_bank(filter_bank, data)
%
% Analysis step for a filter bank
%
% Input
%   filter_bank : convolution masks of the filter bank (as produced by fspecial)
%                 organized as a cell containing filters
%
%   data        : input data
%
% Output
%   result      : cells containing the filtered images
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 3
    domain = 'spectral';
end

result = zeros([size(data), length(filter_bank)],class(data));

for k = 1:length(filter_bank)
  if ndims(data) == 2
    result(:,:,k) = apply_filter(filter_bank{k}, data, domain);
  elseif  ndims(data) == 3
    result(:,:,:,k) = apply_filter(filter_bank{k}, data, domain);
  end
end
