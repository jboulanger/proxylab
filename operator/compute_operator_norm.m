function opnorm = compute_operator_norm(operator,x0,max_iter)
%
%   L = compute_operator_norm(operator, x0)
%
%  Compute the norm of the operator |Ax| < K|x|
%
% Input:
%   operator : struct with field function 'apply' and 'apply_adjoint'
%   x0       : data
%
% Output
%   L        : the norm of the operator
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)
%
% Main idea: the operator norm is equal to the square root of
% the maximum eigenvalue of op* op. Calculate the dominant 
% eigenvector using the power method, then the corresponding
% eigenvalue using the Rayleigh quotient.

if nargin < 3
  max_iter = 500;
end

x1 = x0+1;
k = 1;
while norm(norm(x1(:)-x0(:))) > eps && k < max_iter
  x0 = x1;
  x1 = operator.apply_adjoint(operator.apply(x0));
  x1 = x1/norm(x1(:),2);
  k = k+1;
end

x2 = operator.apply(x1);
opnorm = sqrt(sum(x2(:).^2))/sqrt(sum(x1(:).^2));

