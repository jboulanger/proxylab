function y = apply_stacked(O,x)
%
% y = apply_stacked(O,x)
%
% Apply a matrix of operator to x : [O11 O12; O21 O22] [z1; z2]
% Input :
%  O : a matrix of operator as a 2D cell
%  x : input
%

if ndims(x) == 3
  y = zeros(size(x,1), size(x,2), size(O,1), class(x));
  
  for k = 1:size(O,1)
    for l = 1:size(O,2)
      y(:,:,k) = y(:,:,k) + O{k,l}.apply(x(:,:,l));
    end
  end
elseif ndims(x) == 4
  y = zeros(size(x,1), size(x,2),size(x,3), size(O,1), class(x));
  for k = 1:size(O,1)
    for l = 1:size(O,2)
      y(:,:,:,k) = y(:,:,:,k) + O{k,l}.apply(x(:,:,:,l));
    end
  end
else
    error('Number of dimension not supported (2 or 3)')
end
