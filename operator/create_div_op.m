function operator = create_div_op()
%
% operator = create_div_op()
%
% Input:
%
% Output
%  operator :  struct with field as function 'apply', 'apply_adjoint' and real 'norm'
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

operator.name = 'div';
operator.apply = @(x) imfilter(x{1}, [0 1 -1], 'symmetric') + imfilter(x{2}, [0 1 -1]', 'symmetric');
operator.apply_adjoint = @(x) {imfilter(x, [-1 1 0], 'symmetric'), imfilter(x, [-1 1 0]', 'symmetric')};
operator.norm = 5;
