function operator = create_patch_dictionary_op(patch, window)
%
%  op = create_patch_dictionnary_op(window,patch)
%
%  Create a patch dictionnary operator
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

operator.window = window;
operator.patch = patch;
operator.apply = @(x) apply_patch_dictionary(x, operator.patch, operator.window);
operator.apply_adjoint = @(x) apply_adjoint_patch_dictionary(x, operator.patch, operator.window);
operator.norm = 1;
operator.name = 'P';