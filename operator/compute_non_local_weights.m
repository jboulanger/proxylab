function w = compute_non_local_weights(x, patch, shifts, bandwidth)
%
%  w = compute_non_local_weights(x, patch, shifts, bandwidth)
%
% Computes non local weigths using the L2 distance between image patches in
% a neighbordhood defined by some shifts. The distance is scale by the
% bandwidth and an exponential kernel is used to define the weights:
%
%    w = exp(-d^2/lambda)
%
%
% Code inspired by the NLTV toolbox from Giovanni Chierchia
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

f = fspecial('gaussian', 5 * ceil(patch) + 1, patch);
w = zeros([size(x), size(shifts,1)], class(x));
D = ndims(x);
for k = 1:size(shifts,1)
    tmp = (x - circshift(x, shifts(k,:))).^2;
    tmp = imfilter(tmp, f, 'symmetric');
    if D == 2
      w(:,:,k) = tmp;
    elseif D == 3
      w(:,:,:,k) = tmp;
    else
      error('Undefined number of dimensions (%d).', D);
    end
end

if nargin < 4 % automated weight based on the std of the weights
    bandwidth = std(sqrt(w(:)));
    w = exp(- 0.5 * w / bandwidth^2);
else
    w = exp(- w / bandwidth^2);
end

w = w ./ repmat(sum(w,3), [1, 1, size(w,3)]);
