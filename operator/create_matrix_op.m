function operator = create_matrix_op(A)
%
% operator = create_matrix_op(A)
%
% Input
%   A : matrix
%
% Output
%  operator :  struct with field as function 'apply', 'apply_adjoint' and real 'norm'
%

operator.name = 'A';
operator.matrix = A;
operator.AtA =  A'*A;
operator.I = eye(size(A'*A));
operator.apply = @(x) operator.matrix * x;
operator.apply_adjoint = @(x) operator.matrix' * x;
operator.resolvant = @(x,gamma) (operator.I + gamma * operator.AtA) \ x;
operator.norm = max(svd(operator.AtA));

