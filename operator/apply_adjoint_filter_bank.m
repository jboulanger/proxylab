function result = apply_adjoint_filter_bank(filter_bank, data, domain)
%
% operator = create_convolution_op(filter_bank,data)
%
% Synthesis step for a filter bank.
%
% Note the function does not compute the adjoint of the filter bank.
%
% If filter_bank and data are two dimensional result is one dimensionnal.
%
% Input
%   filter_bank : convolution masks of the filter bank (as produced by fspecial)
%                 organized as a cell containing K filters
%
%   data        : input data(cell of size K)
%
% Output
%   result      : sum_k conj(filter_k) * data
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 3
    domain = 'spectral';
end

% Compute the dimensionality of the result
D = ndims(data) - 1;

if D == 2
   result = zeros(size(data,1), size(data,2),class(data));
   for k = 1:length(filter_bank)
     result = result + apply_filter(filter_bank{k}, data(:,:,k), domain);
   end
elseif D == 3
  result = zeros(size(data,1), size(data,2), size(data,3),class(data));
  for k = 1:length(filter_bank)
    result = result + apply_filter(filter_bank{k}, data(:,:,:,k), domain);
  end
else
     error('Unsupported number of dimensions');
end
