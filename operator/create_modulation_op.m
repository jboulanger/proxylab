function operator = create_modulation_op(M)

operator.name = 'modulation';
operator.mask = M;
if ndims(M) == 3
  operator.apply = @(x) M .* repmat(x,[1 1 size(M,3)]);
  operator.apply_adjoint = @(y) mean(M .* y, 3);
elseif ndims(M) == 4
  operator.apply = @(x) M .* repmat(x,[1 1 1 size(M,4)]);
  operator.apply_adjoint = @(y) mean(M .* y, 4);
end
