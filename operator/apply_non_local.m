function y = apply_non_local(x, shifts)

D = ndims(x);
y = zeros([size(x), size(shifts,1)],class(x));
if D == 2
  for k = 1:size(shifts,1)
    y(:,:,k) = x - circshift(x, shifts(k,:));
  end
elseif D == 3
  for k = 1:size(shifts,1)
    y(:,:,:,k) = x - circshift(x, shifts(k,:));
  end
else
  error('Undefined number of dimensions (%d).', D);
end
