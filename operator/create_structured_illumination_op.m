function operator = create_structured_illumination_op(H,M,zoom,domain,is_otf,dim)
% CREATE_STRUCTURED_ILLUMINATION_OP Create a SIM operator
%
% operator = CREATE_STRUCTURED_ILLUMINATION_OP(H,M,domain,is_otf,dim)
%
% Inputs
%   H : convolution filter mask (as produced by fspecial)
%   M : a modulation mask as a cell
%   domain : 'spectral' (FFT) / 'spatial' (imfilter)
%   is_otf : tells if H is and OTF when domain is 'spectral'
%   dim    : gives the dimensions of the image when is_otf is false and domain is
%            'spectral'
%
% Output
%  operator :  struct with field as function 'apply', 'apply_adjoint' and real 'norm'
%
%  See also create_modulation_op, create_convolution_op,
%  create_resample_op, create_composed_op
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 3
   zoom = 0.5;
end

if nargin < 4
    domain = 'spectral';
end

if nargin < 5
    is_otf = true;
end

if nargin < 6
    dim = size(H);
end

if zoom == 0.5 && strcmpi(domain, 'spectral') && is_otf == true  % fast version
    operator.filter = H;
    operator.mask = M;
    operator.zoom = zoom;
    operator.apply = @(x) apply_structured_illumination(operator.filter, operator.mask, x);
    operator.apply_adjoint = @(x) apply_adjoint_structured_illumination(operator.filter, operator.mask, x);
else
    warning('Slow SIM operator')
    O{1} = create_modulation_op(M);
    O{2} = create_convolution_op(H, 'spectral', is_otf, dim);
    if zoom ~= 1
        O{3} = create_resample_op(zoom, 2, 'spatial');
    end
    operator = create_composed_op(O);
end
operator.name = 'SIM';
operator.norm = 1;%compute_operator_norm(operator, randn(size(operator.filter),class(H)));
