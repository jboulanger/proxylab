function operator = create_filter_bank_op(filter_bank, adjoint_filter_bank, domain, is_otf, dim)
%
% operator = create_filter_bank_op(filter_bank, adjoint_filter_bank, domain, is_otf, dim)
%
% Create a filter bank operator
%
% Input
%   filter_bank : convolution filter mask (as produced by fspecial or OTF or PSF)
%   adjoint_filter_bank : adjoint/reciprocal ()
%
% Output
%  operator :  struct with field as function 'apply', 'apply_adjoint' and real 'norm'
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

compute_adjoint = false;
if nargin < 2
    compute_adjoint = true;
elseif isempty(adjoint_filter_bank)
    compute_adjoint = true;
end

if nargin < 3
    domain = 'spatial';
end

if nargin < 4
    is_otf = false;
end

if ~strcmp(domain,'spectral')
    if compute_adjoint == true
        adjoint_filter_bank = filter_bank;
        for k = 1:length(adjoint_filter_bank)
            adjoint_filter_bank{k} = filter_bank{k}(end:-1:1,end:-1:1,end:-1:1);
        end
    end
else
    if is_otf == false
        for k = 1:length(filter_bank)
            if nargin < 4
                filter_bank{k} = psf2otf(filter_bank{k},dim);
            else
                filter_bank{k} = psf2otf(filter_bank{k});
            end
        end
    end
    if compute_adjoint == true
        adjoint_filter_bank = cell(size(filter_bank));
        for k = 1:length(filter_bank)
            adjoint_filter_bank{k} = conj(filter_bank{k});
        end
    end
end
operator.name = 'filter bank';
operator.domain = domain;
operator.filter_bank = filter_bank;
operator.adjoint_filter_bank = adjoint_filter_bank;
operator.apply = @(x) apply_filter_bank(operator.filter_bank,x,operator.domain);
operator.apply_adjoint = @(x) apply_adjoint_filter_bank(operator.adjoint_filter_bank,x,operator.domain);
operator.norm = length(filter_bank);




