function img = display_patch_dictionary(D, patch, window)
%
% img = apply_adjoint_patch_dictionary(D, patch, window)
%
% Reproject a patch dictionary D on the image domain. The dictionary
% should be rectangular with more elements than its dimension window is
% larger than patch. For example patch 8 and window 16.
%
% Input:
%  D : patch dictionary as a nrow x ncols x window size x patch size
%  patch : patch size; even number
%  window : window size; even number greater than patch
%
% Output:
%  img : the image
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if mod(patch,2) ~= 0
    error('The "patch" parameter should be even.')
end

if mod(window,2) ~= 0
    error('the "window" parameter should be even.')
end

sX = zeros(window/2 * size(D,1), window/2 * size(D,2));
sZ = zeros(window/2 * size(D,1), window/2 * size(D,2));

K = window/2 * (0:size(D,1));
L = window/2 * (0:size(D,2));
k = round(length(K)/2);
l = round(length(L)/2);
        %fprintf(1,'%d %d  // %d %d : %d %d / %d %d \n', m,n,k, l, k, k+window-1, l,l+window-1);
        [cX, cZ] = accumulate_all_patches(D(k,l,:,:), patch, window);
        Kdx = K(k)+1:K(k+2);
        Ldx = L(l)+1:L(l+2);
        sX(Kdx,Ldx) = sX(Kdx,Ldx) + cX;
        sZ(Kdx,Ldx) = sZ(Kdx,Ldx) + cZ;
 
img = sX ./ sZ;


function [X, Z] = accumulate_all_patches(D, patch, window)
X = zeros(window, window);
Z = zeros(window, window);
n = 1;
V = zeros(patch,patch,size(D,3));
%fprintf(1,'%d %d\n', size(X,1) - patch, size(X,2) - patch);
D = reshape(D(1,1,:,:),[size(D,3), size(D,4)]);


for k = 1 : size(X,1) - patch + 1
    for l = 1 : size(X,2) - patch + 1
        %fprintf(1,'  %d %d / %d %d \n', k, k + patch - 1, l, l + patch - 1);
        K = k : k + patch - 1;
        L = l : l + patch - 1;
        X(K,L) = X(K,L) + reshape(D(n,:), [patch, patch]);        
        Z(K,L) = Z(K,L) + 1;        
        n = n + 1;        
    end
end


[~,~,U] = svd(D);
U = reshape(U,patch,patch,patch*patch);
figure(1)
N = round(sqrt(size(D,3)));
subplot(121),imshow(X./Z,[]);
subplot(122),imshow(append_tiles(U,patch,patch),[])




