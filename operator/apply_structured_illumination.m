function y = apply_structured_illumination(H,M,x)
%
% y = apply_structured_illumination(A,M,x)
%
% Apply the structured illumination operator (2D + downsampling 1/2)
%
%  y_k = H M_k x, k in [1,K]
%
% Input:
%  H : OTF
%  M : modulation mask
%  x : input
%
% Ouput:
%  y : SIM images
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

r0 = size(x,1) / 4;
c0 = size(x,2) / 4;
tmp = M .* repmat(x, [1 1 size(M,3)]);
tmp = fft2(tmp) .* repmat(H, [1 1 size(M,3)]);
y = zeros(size(x,1)/2, size(x,2)/2, size(M,3),class(x));
y(1:r0,1:c0,:) = tmp(1:r0,1:c0,:);
y(1:r0,end-c0+1:end,:) = tmp(1:r0,end-c0+1:end,:);
y(end-r0+1:end,1:c0,:) = tmp(end-r0+1:end,1:c0,:);
y(end-r0+1:end,end-c0+1:end,:) = tmp(end-r0+1:end,end-c0+1:end,:);
y = real(ifft2(y));
