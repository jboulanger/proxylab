function M = create_modulation_masks(dimensions, frequencies, amplitudes, angles, phases)
%
%  M = create_modulation_masks(dimensions, frequencies, amplitudes, angles, phases)
%
% Input:
%  dimensions  : array of sizes of the modulations
%  frequencies : array of modulation frequencies
%  amplitudes  : array of modulation amplitudes
%  angles      : array of modulation angles
%  phases      : array of modulation phases
%
% Output:
%  M : modulation mask (1+a cos(frequency (cos(angle) x + sin(angle) y) + phase))
%      with x and y given by meshgrid meshgrid(1:width, 1:height);
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

M = zeros([dimensions length(frequencies)]);

[x, y] = meshgrid(1:dimensions(2), 1:dimensions(1));

for k = 1:length(frequencies(:))
  kx = frequencies(k) .* cos(angles(k));
  ky = frequencies(k) .* sin(angles(k));
  M(:,:,k) = 1 + amplitudes(k) * cos(kx .* x + ky .* y + phases(k));
end
