function D = apply_patch_dictionary(img, patch, window)
%
% D = apply_patch_dictionary(img, patch, window)
%
% Extract a patch dictionary for each window in the image. Windows are
% overlapping by 1/2. Typically the window size is larger than the patch
% size. Moreover, only even size are allowed.
%
% Input:
%  img    : image
%  patch  : patch size
%  window : window size
%
% Output:
%  D : the 4D dictionary of size [2*size(img)/window, (window-patch+1)^2, patch^2 ]
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if mod(patch,2) ~= 0
    error('The "patch" parameter should be even.')
end

if mod(window,2) ~= 0
    error('the "window" parameter should be even.')
end

patch_size = patch^2; % number of pixel in the patch
window_size = (window - patch + 1)^2; % number of patches in the window

D = zeros([2 * size(img) / window, window_size, patch_size],class(img));

K = window/2 * (0:size(D,1));
L = window/2 * (0:size(D,2));

for k = 1:length(K)-2
    for l = 1:length(L)-2
        %fprintf(1,'%d %d  // %d %d : %d %d / %d %d \n', m,n,k, l, k, k+window-1, l,l+window-1);
        crop = img(K(k)+1:K(k+2), L(l)+1:L(l+2));
        D(k, l, :, :) = extract_all_patches(crop, patch);
    end
end

function D = extract_all_patches(img, patch)
D = zeros(prod(size(img) - patch + 1), patch^2, class(img));
n = 1;
for k = 1 : size(img,1) - patch + 1
    for l = 1 : size(img,2) - patch + 1
       % fprintf(1,'  %d %d / %d %d \n', k, k + p - 1, l, l + p - 1);
        P = img(k : k + patch - 1, l : l + patch - 1);
        D(n, :) = P(:);
        n = n + 1;
    end
end
