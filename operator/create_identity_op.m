function operator = create_identity_op(factor)
%
% operator = create_identity_op(factor)
%
% Output
%  operator :  struct with field as function 'apply', 'apply_adjoint' and real 'norm'
%

if nargin < 1
   factor = 1;
end

operator.name = 'I';
operator.apply = @(x) factor .* x;
operator.apply_adjoint = @(x) factor .* x;
operator.norm = factor;
