function y = apply_filter(f,x,domain)
%
% y = apply_filter(f,x,domain)
%
% Apply the filter f to the image x either in spatial or in Fourier domain
%
% The filter can be a PSF in the spatial domain of a OTF in the
% spectral domain
%

if isempty(f)
  y = x;
else
  if ndims(f) == ndims(x) % if x and f have the same dimensions
    y = do_apply_filter(f,x,domain);
  elseif ismatrix(f) && ndims(x) == 3 % 2D filter on 3D data
    y = zeros(size(x),class(x));
    for k = 1:size(x,3)
      y(:,:,k) = do_apply_filter(f,x(:,:,k),domain);
    end
  elseif ndims(f) == 3 && ndims(x) == 4 % 3D filter on 4D data
    y = zeros(size(x),class(x));
    for k = 1:size(x,4)
      y(:,:,:,k) = do_apply_filter(f,x(:,:,:,k),domain);
    end
  end
end


function y = do_apply_filter(f,x,domain)
if ~strcmp(domain, 'spectral')
  y = imfilter(x, f);
else
  y = real(ifftn(fftn(x) .* f));
end
