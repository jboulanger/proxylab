function x = apply_adjoint_structured_illumination(H,M,y)
%
% y = apply_adjoint_structured_illumination(H,M,y)
%
% Apply the structured illumination adjoint operator (2D + downsampling 1/2)
%
%   y = sum_k M_k H y_k
%
% Input:
%  H : the adjoint of the OTF in Fourier domain or PSF as a mask
%  M : modulation mask
%  x : input
%  domain: is equals to 'spectral' the A is considered as an OTF and the
%  computation are done using the FFT
%
%

r = size(y,1);
c = size(y,2);
r0 = round(r / 2);
c0 = round(c / 2);
X = zeros(2 * r, 2 * c, size(M,3),class(y));
tmp = fft2(y);
X(1:r0,1:c0,:) = tmp(1:r0,1:c0,:);
X(1:r0,end-c0+1:end,:) = tmp(1:r0,end-c0+1:end,:);
X(end-r0+1:end,1:c0,:) = tmp(end-r0+1:end,1:c0,:);
X(end-r0+1:end,end-c0+1:end,:) = tmp(end-r0+1:end,end-c0+1:end,:);
X = real(ifft2(X .* repmat(H, [1,1,size(M,3)])));
X = X .* M;
x = mean(X,3);
