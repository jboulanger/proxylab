function y = apply_resample(x,zoom,dimension,domain)
% APPLY_RESAMPLE Resample the image
% y = APPLY_RESAMPLE(x,zoom,dimension,domain)
% y = APPLY_RESAMPLE(x,zoom,dimension)
% y = APPLY_RESAMPLE(x,zoom)
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 3
    dimension = ndims(x);
end

if nargin < 4
    domain = 'spectral';
end

if zoom == 1
    y = x;
else
    if ismatrix(x)
        if zoom == 0.25
            if strcmp(domain,'spatial')
                y = imfilter(x, 0.0625 * ones(4,4), 'symmetric');
                y = y(2:4:end,2:4:end);
            else
                y = fftshift(fft2(x));
                y = y(size(y,1)/4+1:3*size(y,1)/4, size(y,2)/4+1:3*size(y,2)/4);
                y = real(ifft2(fftshift(y)));
            end
        elseif zoom == 0.5 % downsample x 1/2
            if strcmp(domain,'spatial')
                y = imfilter(x, .25*ones(2,2), 'symmetric');
                y = y(2:2:end,2:2:end);
            else
                y = fftshift(fft2(x));
                y = y(size(y,1)/4+1:3*size(y,1)/4, size(y,2)/4+1:3*size(y,2)/4);
                y = real(ifft2(fftshift(y)));
            end
        elseif zoom == 2 % up sample x 2
            if strcmp(domain,'spatial')
                %K = round((1:2*size(x,1))/2);
                %L = round((1:2*size(x,2))/2);
                %y = x(K,L);
                y = kron(x, ones(2,2));
            else
                y = real(ifft2(fftshift(padarray(fftshift(fft2(x)),size(x)/2))));
            end
        elseif zoom == 4
            %K = round((1:4*size(x,1))/4);
            %L = round((1:4*size(x,2))/4);
            %y = x(K,L);
            y = kron(x, ones(4,4));
        else
            error('Unsupported zoom (%f).', zoom);
        end
    elseif ndims(x) == 3
        if dimension == 3
            if zoom == 0.5 % downsample x 1/2
                y = imfilter(x, 0.125 * cat(3,[1 1;1 1], [1 1;1 1]));
                y = y(1:2:end-1,1:2:end-1,1:2:end-1);
            elseif zoom == 2 % up sample x 2
                K = round((1:2*size(x,1))/2);
                L = round((1:2*size(x,2))/2)';
                % M = reshape(round((1:2*size(x,3))/2), [1,1,2*size(x,3)]);
                y = x(K,L); % note: kron does not work in 3D
            else
                error('Unsupported zoom (%f).', zoom);
            end
        else % apply the resampling in 2D for each slice
            y = zeros(zoom * size(x,1), zoom * size(x,2), size(x,3));
            for k = 1:size(x,3)
                y(:,:,k) = apply_resample(x(:,:,k), zoom, 2, domain);
            end
        end
    else
        error('Unsupported number of dimensions (%d).', ndims(x));
    end
end
