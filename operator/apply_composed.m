function y = apply_composed(O,x)
%
% y = apply_composed(O,x)
%
% Apply a list of operator to x : O_n...O_1 x
% Input :
%  O : a list of operator as a cell
%  x : input
%
y = x;
for k = 1:length(O)
    y = O{k}.apply(y);
end
