function operator = create_non_local_op(img, patch, window)
%
% operator = create_non_local_op(A, sigma, lambda)
%
% Input
%  shifts : list of shifts as a N X D array
%  img    : reference image on which we can compute patches
% Output
%  operator :  struct with field as function 'apply', 'apply_adjoint' and real 'norm'
%

shifts = zeros((2 * window + 1)^2-1,2);
k = 1;
for i = -window:window
    for j=-window:window
        if i ~=0 || j ~=0
            shifts(k,:) = [i,j];
            k = k + 1;
        end
    end
end

operator.patch = patch;
operator.shifts = shifts;
operator.weights = compute_non_local_weights(img, patch, shifts);
operator.apply = @(x) operator.weights .* apply_non_local(x, operator.shifts);
operator.apply_adjoint = @(x) apply_adjoint_non_local(operator.weights .* x, operator.shifts);
operator.norm = size(shifts, 1);
operator.name = 'non-local';
operator.norm = compute_operator_norm(operator, randn(size(img)));
