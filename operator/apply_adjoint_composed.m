function x = apply_adjoint_composed(O,y)
%  x = apply_adjoint_composed(O,y)
% 
% Apply the adjoint of a composed operator defined by a cell array of
% operator.
%
x = y;
for k = length(O):-1:1
    x = O{k}.apply_adjoint(x);
end
