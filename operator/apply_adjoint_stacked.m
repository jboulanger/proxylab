function x = apply_adjoint_stacked(O,y)
%
% y = apply_stacked(O,x)
%
% Apply a matrix of operator to x : [O11' O21'; O12' O22']' [z1; z2]
% Input :
%  O : a matrix of operator as a 2D cell
%  x : input whos last dimension correspond to the
%

% matrix multiplication with transposed cell array
if (ndims(y) == 3 && size(O,1) > 1) || (ndims(y) == 2 && size(O,1) == 1)
  x = zeros(size(y,1), size(y,2), size(O,2), class(y));
  for k = 1:size(O,1)
    for l = 1:size(O,2)
      x(:,:,l) = x(:,:,l) + O{k,l}.apply_adjoint(y(:,:,k));
    end
  end
elseif (ndims(y) == 4 && size(O,1) > 1) || (ndims(y) == 3 && size(O,1) == 1)
  x = zeros(size(y,1), size(y,2), size(y,3), size(O,2), class(y));
  for k = 1:size(O,1)
    for l = 1:size(O,2)
      x(:,:,:,l) = x(:,:,:,l) + O{k,l}.apply_adjoint(y(:,:,:,k));
    end
  end
else
    error('Number of dimension (%d) not supported (2 or 3)', ndims(y))
end
