function img = apply_adjoint_patch_dictionary(D, patch, window)
%
% img = apply_adjoint_patch_dictionary(D, patch, window)
%
% Reproject a patch dictionary D on the image domain. The dictionary
% should be rectangular with more elements than its dimension window is
% larger than patch. For example patch 8 and window 16.
%
% Input:
%  D : patch dictionary as a nrow x ncols x window size x patch size
%  patch : patch size; even number
%  window : window size; even number greater than patch
%
% Output:
%  img : the image
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if mod(patch,2) ~= 0
    error('The "patch" parameter should be even.')
end

if mod(window,2) ~= 0
    error('the "window" parameter should be even.')
end

sX = zeros(window/2 * size(D,1), window/2 * size(D,2), class(D));
sZ = zeros(window/2 * size(D,1), window/2 * size(D,2), class(D));

K = window/2 * (0:size(D,1));
L = window/2 * (0:size(D,2));
for k = 1:length(K)-2
    for l = 1:length(L)-2
        %fprintf(1,'%d %d  // %d %d : %d %d / %d %d \n', m,n,k, l, k, k+window-1, l,l+window-1);
        [cX, cZ] = accumulate_all_patches(D(k,l,:,:), patch, window);
        Kdx = K(k)+1:K(k+2);
        Ldx = L(l)+1:L(l+2);
        sX(Kdx,Ldx) = sX(Kdx,Ldx) + cX;
        sZ(Kdx,Ldx) = sZ(Kdx,Ldx) + cZ;
    end
end

img = sX ./ sZ;

function [X, Z] = accumulate_all_patches(D, patch, window)
X = zeros(window, window,class(D));
Z = zeros(window, window,class(D));
n = 1;
%fprintf(1,'%d %d\n', size(X,1) - patch, size(X,2) - patch);

for k = 1 : size(X,1) - patch + 1
    for l = 1 : size(X,2) - patch + 1
        %fprintf(1,'  %d %d / %d %d \n', k, k + patch - 1, l, l + patch - 1);
        K = k : k + patch - 1;
        L = l : l + patch - 1;
        X(K,L) = X(K,L) + reshape(D(1,1,n,:), [patch, patch]);
        Z(K,L) = Z(K,L) + 1;
        n = n + 1;
    end
end
