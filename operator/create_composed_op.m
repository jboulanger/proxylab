function operator = create_composed_op(O)
%
% operator = create_composed_op(O)
%
% Create an operator from a list of operators
%
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

operator.name = 'composed';
operator.list = O;
operator.apply = @(x) apply_composed(operator.list, x);
operator.apply_adjoint = @(x) apply_adjoint_composed(operator.list, x);
operator.norm = 1;
