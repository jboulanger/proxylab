function operator = create_resample_op(zoom, D, domain)

if nargin < 1
   zoom = 2;
end

if nargin < 2
   D = 2;
end

if nargin < 3
   domain = 'spectral';
end


operator.name = 'resample';
operator.zoom = zoom;
operator.dimension = D;
operator.domain = domain;
operator.apply = @(x) apply_resample(x, operator.zoom, operator.dimension, operator.domain);
operator.apply_adjoint = @(x) apply_resample(x, 1/operator.zoom, operator.dimension,operator.domain);
operator.norm = 1;
