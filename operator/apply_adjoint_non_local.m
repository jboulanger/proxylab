function x = apply_adjoint_non_local(y, shifts)

sz = size(y);
D = ndims(y) - 1;
x = zeros(sz(1:end-1),class(y));
for k = 1:size(shifts,1)
    if D == 2
      yk = y(:,:,k);
    else
      yk = y(:,:,:,k);
    end
    x = x + yk - circshift(yk, -shifts(k,:));
end
