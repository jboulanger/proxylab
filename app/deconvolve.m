function [img, etime, niter, rnorm] = deconvolve(H, zoom, data, data_term, regularization, lambda, opts)

if nargin < 7
    opts.max_iter = 500;
end

if ~isfield(opts, 'max_iter')
    opts.max_iter = 500;
end

t0 = tic;

O{1} = create_convolution_op(H, 'spectral', true);
if zoom ~= 1
  O{2} = create_resample_op(zoom, 2, 'spatial');
end
A = create_composed_op(O);

if ~isfield(opts,'init')
    if ~isfield(opts,'init_iter')
        opts.init_iter = 20;
    end

    x0 = A.apply_adjoint(data);
    for iter = 1:opts.init_iter
        x0 = max(0, x0 - A.apply_adjoint(A.apply(x0) - data));
    end
    opts.init = x0;
    etime = toc(t0);
    img = x0;
end

if opts.max_iter > 0 && nargin > 4
    if strcmpi(data_term, 'poisson') || strcmpi(data_term, 'kl')
        cost(1) = create_cost_term(create_kullback_leibler_fun(100,data), A);
    elseif strcmpi(data_term, 'gaussian') || strcmpi(data_term, 'l2')
        cost(1) = create_cost_term(create_l2norm_fun(1,data), A);
    else % weighted l2
        [~, weights] = analyze_ccd_noise(mean(data,3));
        weights = max(weights(:)) ./ weights;
        weights = repmat(weights,1,1,size(data,3));
        cost(1) = create_cost_term(create_l2norm_fun(1, data, weights), A);
    end

    if ~isfield(opts, 'init_nltv')
        opts.init_nltv = opts.init;
    end
    cost(2) = create_regularization_term(lambda, regularization, opts.init_nltv);

    opts.bounds = [0, Inf];
    output = pdhg(cost, opts);
    img = output.estimate;
    etime = toc(t0); % do not use output.elapsed_time as it is not recorded
    niter = output.iteration;
else
    niter = 0;
end

residuals = data - A.apply(x0);
rnorm = sqrt(mean(residuals(:).^2)) / std(data(:).^2);
