function [img, etime, niter, rnorm] = reconstruct_sim(H, M, zoom, data, data_term, regularization, lambda, opts)
% RECONSTRUCT_SIM Reconstruct SIM data
%
%  [img,etime, niter,rnorm] = RECONSTRUCT_SIM(H,M,data,data_term,regularization,lambda,opts)
%
% Reconstruct SIM images using the a primal dual algorithm
%
% data_term : data term term ( 'Poisson' or 'Gaussian' or 'weighted l2')
% regularization : regularization term
%                 ('tikhonov','tv','nltv','schatten-hessian', 'schatten-patch')
% lambda : regularization parameter
% opts   : a struct passed to the optimization procedure
%
% Jerome Boulanger (jerome.boulanger@curie.fr)

if nargin < 8
    opts.max_iter = 1000;
end

if ~isfield(opts, 'max_iter')
    opts.max_iter = 1000;
end

if ~isfield(opts, 'rho')
    opts.rho = 2;
end

if ~isfield(opts, 'tau')
    opts.tau = 0.1;
end

if ~isfield(opts, 'sigma')
    opts.sigma = 1;
end
t0 = tic;

A = create_structured_illumination_op(H, M, zoom, 'spectral', true);

if ~isfield(opts,'init')
    opts.init = zeros(size(A.apply_adjoint(data)));
end

if opts.max_iter > 0 && nargin > 4
    if strcmpi(data_term, 'poisson') || strcmpi(data_term, 'kl')
        if ~isfield(opts,'noise')
            p = analyze_ccd_noise(sum(data,3));
            fprintf(1, 'Estimated noise parameters\n gain:%f\n offset:%f\n', p(1), p(2));
            cost(1) = create_cost_term(create_kullback_leibler_fun(10, data, p(1), p(2)), A);
        else
            gain = opts.noise.gain;
            background = opts.noise.m0;
            sigma0 = opts.noise.sigma0;
            cost(1) = create_cost_term(create_kullback_leibler_fun(100, data, gain, background, sigma0), A);
        end
    elseif strcmpi(data_term, 'gaussian') || strcmpi(data_term, 'l2')
        cost(1) = create_cost_term(create_l2norm_fun(1,data), A);
    else % weighted l2
        flt = data;
        for k=1:size(data,3)
            flt(:,:,k) = medfilt2(data(:,:,k), [7,7]);
        end
        if ~isfield(opts,'noise')
            p = analyze_ccd_noise(sum(data,3));
            fprintf(1, 'Estimated noise parameters\n gain:%f\n offset:%f\n', p(1), p(2));
            weights = sqrt(p(1) .* flt + p(2));
        else
            weights = sqrt(opts.noise.gain .* flt + opts.noise.sigma0^2 - opts.noise.gain * opts.noise.m0);
        end
        weights = weights ./ mean(weights(:));
        cost(1) = create_cost_term(create_l2norm_fun(1, data, weights), A);
    end
       
    if ~isfield(opts, 'init_nltv')
        opts.init_nltv = opts.init;
    end
    cost(2) = create_regularization_term(lambda, regularization, opts.init_nltv);
    if ~isfield(opts, 'bounds')
        opts.bounds = [0 Inf];
    end
       
    output = pdhg(cost, opts);
    img = output.estimate;
    etime = toc(t0); 
    niter = output.iteration;
else
    niter = 0;
    img = opts.init;
end

residuals = data - A.apply(img);
rnorm = sqrt(mean(residuals(:).^2)) / std(data(:).^2);
