% Structured Illumination microscopy benchmarking
%
% In structured illumination the sample is modulated by an illumination
% pattern and blurred by the optical transfer of the microscope.  High
% resolution image can be obtained by restoring the data acquired with
% various modulation patterns.
%
% This example incorporate a realistic Poisson-Gaussian noise model and
% demonstrate how to manually interact with the minimization procedure.
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

disp('Structured illumination benchmark');
clear all; close all;
setup_proxylab_toolbox();
addpath('../app')

%% Create a test dataset
period = 4.75; % modulation period (before zoom)
cutoff = 4; % cutoff frequency in pixels
zoom = 2; % zoom factor (1 or 2)
gain = 0.01;% gain of the sensor (max(obj)==1 => max photon==100)
sigma = 0; % readout noise
offset = 0; % camera offset

obj = generate_test_image('fibers',256); % generate a test image
p = generate_sim_parameters(period,3,3); % generate SIM parameters
m = generate_sim_modulation(size(obj), p); % generate a set of modulation
otf = generate_otf(size(obj,1), cutoff); % compute an OTF
data = generate_sim_data(obj,m,otf,zoom,gain,sigma,offset); % generate SIM data
subplot(121), imshow(append_tiles(data,3,3),[]);
wide_field = apply_resample(mean(data,3), zoom, 2, 'spectral');
subplot(122), imshow(wide_field,[]);

%% Reconstruct the dataset
dataterm_str = {'gaussian','poisson','Weighted L2'};
regularization_str = {'l2-laplacian','tv','schatten-hessian','nltv'};
opts.max_iter = 300;
% set the noise parameters
opts.noise.gain = 0.01;
opts.noise.sigma0 = 0;
opts.noise.m0 = 0;
opts.init = wide_field;
tic
parfor i = 1:length(dataterm_str)
    for j = 1:length(regularization_str)
        u = reconstruct_sim(otf,m,0.5,data,dataterm_str{i},regularization_str{j},0.0025,opts);
        subplot(length(dataterm_str),length(regularization_str),j+(i-1)*length(regularization_str))
        imshow(u,[])
        title(sprintf('%s + %s\npsnr:%.2fdB', dataterm_str{i}, regularization_str{j}, psnr(obj,u,1)));
    end
end
toc
