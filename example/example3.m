% Deconvolution and up-sampling
%
%  argmin_x 1/2 |Hx-y|^2 + |Hessian(x)|_*
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)
disp('Deconvolution and up-sampling example');
clear all; close all;
setup_proxylab_toolbox();
use_gpu = true;

%% Create the problem
N = 256;
img = 255*generate_test_image('fibers',N);
H = generate_otf(N,3);
noise_std = 0.001; % noise level
data = fftshift(fft2(img) .* H);
data = ifftshift(data(65:end-64,65:end-64)); % down-sampling by a factor of 2
data = real(ifft2(data)) + noise_std * randn(size(data));
clf;
subplot(221), imshow(img,[]), title('Original image');
subplot(222), imshow(data,[]), title('Data image');
subplot(223), imshow(log(0.1+fftshift(H)),[]), title('OTF (log scale)');
subplot(224), imshow(sqrt(otf2psf(H)),[]), title('PSF (gamma=2)');

data = to_cuda(data,use_gpu);
H = to_cuda(H,use_gpu);

%% define the cost function to optimize
disp('  starting reconstruction...')
lambda = 0.1; % regularization parameter
O{1} = create_convolution_op(H,'spectral',true);
O{2} = create_resample_op(0.5,2,'spectral');
A = create_composed_op(O);
cost(1) = create_cost_term(create_l2norm_fun(1,data), A);
% try here other regularization terms (tv, l2-laplacian,schatten-patch)
cost(2) = create_regularization_term(lambda, 'tv', data);

opts.max_iter = 200;
opts.record = 1;
opts.tau = 0.5;
opts.sigma = 10;
opts.rho = 1.9;
opts.observer = @progressbar;
opts.bounds = [0, Inf];
output = pdhg(cost, opts);
fprintf('  finished in %f seconds (%.2fdB).\n', max(output.elapsed_time), cpsnr(output.estimate, img));

% Display the result
clf;
subplot(221)
imshow(img,[])
title('Original image');

subplot(222)
data_up = apply_resample(data,2,2,'spectral');
imshow(data_up,[])
title(sprintf('Data (%.2fdB)', cpsnr(data_up,img)));

subplot(223)
imshow(output.estimate,[])
title(sprintf('Deconvolved (%s) (%.2fdB)', output.options.algorithm, cpsnr(output.estimate,img)));


% Plot cost functions
if isfield(output,'cost') && isfield(output,'cost')
    subplot(224)      
    %x = output.elapsed_time';
    x = (1:size(output.cost,1))';
    y = sum(output.cost, 2);
    p = polyfit(log10(x(10:end)), log10(y(10:end)),1);
    f = 10.^polyval(p, log10(x));
    loglog(x, y, x, f);
    xlabel('Time [s]')
    ylabel('Cost function')
    title(sprintf('Convergence (slope:%f)', p(1)))
    fprintf('  convergence (slope:%f)\n', p(1))
    axis tight
    axis square
    grid on
end


