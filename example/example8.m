% Mixed noised deconvolution example
%
% variational model
% J(u,v) = l2(v-f) + lambda1 DKL(u,v) + lambda2 TV(u)
%
% https://arxiv.org/pdf/1611.00690.pdf
%
clear all; close all;
setup_proxylab_toolbox();
rng(1)
use_gpu = 0;
%% create the deconvolution problem
disp('Mixed noised deconvolution example');
N = 64;
img = 255 * generate_test_image('steps',N,4);
H = generate_otf(N,3);

%data = real(ifft2(fft2(img) .* H));
%data = real(ifft2(fft2(img) .* H)) + noise_std * randn(size(img));

noise_std = 2; % noise level
data = poissrnd(real(ifft2(fft2(img) .* H))) + noise_std * randn(size(img));

clf;
subplot(221), imshow(img,[]), title('Original image'); colorbar
subplot(222), imshow(data,[]), title('Data image'); colorbar
subplot(223), imshow(log(0.1+fftshift(H)),[]), title('OTF (log scale)'); colorbar
subplot(224), imshow(sqrt(otf2psf(H)),[]), title('PSF (gamma=2)'); colorbar
drawnow
%%  copy arrays to the GPU
data = to_cuda(data,use_gpu);
H = to_cuda(H);

%% define the cost function to optimize
disp('  starting reconstruction...')
lambda1 = 2; % Gaussian fidelity parameter
lambda2 = 100; % KL fidelity parameter

bounds = [1,256];

% first term: l2(v-f) = l2([0,1;0 0][u;v] - [f;0])
T1 = create_stacked_op({create_identity_op(0), create_identity_op(1);...
    create_identity_op(0), create_identity_op(0)});
datal2 = zeros([size(data),2]);
datal2(:,:,1) = data;
cost(1) = create_cost_term(create_l2norm_fun(lambda1,datal2), T1);

% second term: DKL(Hu,v) = DKL([H,0;0,1][u;v])
T2 = create_stacked_op({create_convolution_op(H,'spectral',true), create_identity_op(0); create_identity_op(0), create_identity_op(1)});
cost(2) = create_cost_term(create_kullback_leibler_uv_fun(lambda2,bounds), T2);

% third term: TV(u) = l1([Dx 0;Dy 0] [u;v])
% divide by the following so that the stacked operator has norm 1
den = sqrt(8); 
Dx = [-1, 1, 0]/den;
Dy = [-1; 1; 0]/den;
T3 =  create_stacked_op({create_convolution_op(Dx), create_identity_op(0);...
                        create_convolution_op(Dy), create_identity_op(0)});
cost(3) = create_cost_term(create_l1norm_fun(10), T3);

% Compute operator norm
f0 = zeros([size(data),2]);
f0(:,:,1) = randn(size(data));
f0(:,:,2) = randn(size(data));
compute_operator_norm(cost(1).operator,f0)
compute_operator_norm(cost(2).operator,f0)
compute_operator_norm(cost(2).operator.list{1,1},randn(size(data)))
compute_operator_norm(cost(3).operator,f0)
L = compute_multi_operator_norm(cost,f0)
%L = 2;

opts = [];
opts.init = repmat(data,[1 1 2]);% need to initialize with proper dimensions
opts.observer = @progressbar_cost;
opts.max_iter = 10000;
opts.record = 50;
opts.sigma = 0.1;
opts.normL = L;
opts.tau = 1/(opts.sigma * opts.normL);
opts.rho = 0.9;
opts.bounds = bounds;
output = pdhg(cost, opts);
estimate_u = squeeze(gather(output.estimate(:,:,1)));
estimate_v = squeeze(gather(output.estimate(:,:,2)));
fprintf('  finished in %f seconds (%.2fdB).\n', max(output.elapsed_time), psnr(img, estimate_u, 255));

% Display the result
clf;
subplot(241)
imshow(img,[])
title('Original image');
colorbar

subplot(245)
imshow(data,[])
title(sprintf('Data f (%.2fdB)', psnr(img,gather(data),255)));
colorbar

subplot(242)
imshow(estimate_u,[])
title(sprintf('Deconvolved u (%s) (%.2fdB)', output.options.algorithm, psnr(img,estimate_u,255)));
colorbar

subplot(246)
imshow(estimate_v,[]);
title('v (data without Gaussian noise)');
colorbar

subplot(243)
imshow(data-estimate_v,[]);
title('data - v (estimated Gaussian noise)');
colorbar

Hu = real(ifft2(fft2(estimate_u) .* H));
subplot(247)
imshow(estimate_v - Hu,[]);
title('v - Hu (estimated Poisson noise)');
colorbar

%%
% Plot cost functions
figure(2)
subplot(221)
loglog(output.iteration, output.pd_gap/(numel(data)))
title('Primal dual gap / npx')

if isfield(output,'cost') && isfield(output,'cost')
  subplot(224)
  loglog(output.iteration, sum(output.cost,2));hold on;   
  xlabel('Iterations')
  ylabel('Cost function')
  title('Convergence')  
  axis tight
  grid on
end
