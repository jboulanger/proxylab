%% LASSO problem with $|Ax-b| + lambda * |x|$
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

clear all; close all;
setup_proxylab_toolbox();
%%
disp('Lasso')

A = rand(5,10);
x = rand(size(A,2),1);
b = A * x;
lambda = .0001;

opts.max_iter = 3000;
opts.record = 1;
%opts.init = A'*b;

%% create the cost functions
% The data term $|Ax-b|^2$ using a function involving a linear operator
% Here $f(.) = |A.-b|^2$ and $T = I$
cost(1) = create_cost_term(create_quadratic_linear_fun(1,A,b));
% The L1 norm term |x|
cost(2) = create_cost_term(create_l1norm_fun(lambda));

%% Using FISTA algorithm
disp('Running FISTA')
output1 = fista(cost, opts);
fprintf(1, '  norm of the error %g\n', norm(output1.estimate - x) / norm(x));
fprintf(1, '  elapsed time %.2fs\n', output1.elapsed_time(end));

%% Using PPXA algorithm
disp('Running PPXA')
output2 = ppxa(cost, opts);
fprintf(1, '  norm of the error %g\n', norm(output2.estimate - x) / norm(x));
fprintf(1, '  elapsed time %.2fs\n', output2.elapsed_time(end));

%% Using primal-dual algorithms
% Instead of using a prox of |Ax-b|²,  we use f(.) = |.-b|² and T = A
cost(1) = create_cost_term(create_l2norm_fun(1, b), create_matrix_op(A));

%% using chambolle - pock
disp('Running chambolle - pock')
opts.tau = 0.1;
output3 = chambolle_pock([cost(2) cost(1)], opts);
fprintf(1, '  norm of the error %f\n', norm(output3.estimate - x) / norm(x));
fprintf(1, '  elapsed time %.2fs\n', output3.elapsed_time(end));

%% Using the PDHG algorithm (multi-term chambolle-pock with no smooth term)
disp('Running PDGH')
L = compute_multi_operator_norm(cost,randn(size(x)));
opts.rho = 1;
opts.tau = 0.1;
opts.sigma = 10;
opts.bounds = [0,1];
opts.TolX = 1e-16;
output4 = pdhg(cost, opts);
fprintf(1, '  norm of the error %f\n', norm(output4.estimate - x) / norm(x));
fprintf(1, '  elapsed time %.2fs\n', output4.elapsed_time(end));

% Display cost functions
subplot(121)
loglog(sum(output1.cost,2),'b','LineWidth',2);
hold on
loglog(sum(output2.cost,2),'g','LineWidth',2);
loglog(sum(output3.cost,2),'y','LineWidth',2);
loglog(sum(output4.cost,2),'r','LineWidth',2);
loglog([1,100],lambda * norm(x,1) * [1 1],'k--')
hold off
legend('FISTA','PPXA','CHAMBOLE-POCK','PDHG')
ylabel('Cost')
xlabel('Time [s]')
title('Lasso')
axis tight
subplot(122)
loglog(output1.elapsed_time,sum(output1.cost,2),'b','LineWidth',2);
hold on
loglog(output2.elapsed_time,sum(output2.cost,2),'g','LineWidth',2);
loglog(output3.elapsed_time,sum(output3.cost,2),'y','LineWidth',2);
loglog(output4.elapsed_time,sum(output4.cost,2),'r','LineWidth',2);
hold off
legend('FISTA','PPXA','CHAMBOLE-POCK','PDHG')
xlabel('Time [s]')
ylabel('Cost')
title('Lasso')
axis tight
