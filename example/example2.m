% Deconvolution
%
%  argmin_x 1/2 |Hx-y|^2 + |Hessian(x)|_*
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

clear all; close all;
setup_proxylab_toolbox();
use_gpu = true;
%% create the deconvolution problem
disp('Deconvolution example');
N = 128;
img = 255*generate_test_image('fibers',N);
H = generate_otf(N,4);
%H = psf2otf(fspecial('gaussian',[11 11], 1.5),[N,N]);
noise_std = 2; % noise level
data = real(ifft2(fft2(img) .* H)) + noise_std * randn(size(img));
clf;
subplot(221), imshow(img,[]), title('Original image');
subplot(222), imshow(data,[]), title('Data image');
subplot(223), imshow(log(0.1+fftshift(H)),[]), title('OTF (log scale)');
subplot(224), imshow(sqrt(otf2psf(H)),[]), title('PSF (gamma=2)');
drawnow
data = to_cuda(data, use_gpu);
H = to_cuda(H, use_gpu);

%% define the cost function to optimize
disp('  starting reconstruction...')
lambda = .1; % regularization parameter
cost(1) = create_cost_term(create_l2norm_fun(1,data), create_convolution_op(H,'spectral',true));
% try here other regularization terms (tv, l2-laplacian,l1-hessian,schatten-hessian,schatten-patch)
cost(2) = create_regularization_term(lambda,'l1-hessian', data);
L = compute_multi_operator_norm(cost,randn(size(data)));
opts = [];
opts.max_iter = 5000;
opts.TolX = 1e-6;
opts.TolGap = 1e-6;
opts.observer = @progressbar;
opts.init = data;
opts.record = 10;
opts.use_gap = true;
opts.rho = 0.9;
opts.sigma = 0.2;
opts.tau = 1/(opts.sigma*L);
opts.bounds = [0, 256];
output = pdhg(cost, opts);
fprintf('  finished in %f seconds (%.2fdB).\n', ...
    max(output.elapsed_time), cpsnr(output.estimate,img));

% Display the result
clf;
subplot(221)
imshow(img,[])
title('Original image');
colorbar

subplot(222)
imshow(data,[])
title(sprintf('Data (%.2fdB)', cpsnr(data,img)));
colorbar

subplot(223)
imshow(output.estimate,[])
title(sprintf('Deconvolved (%s) (%.2fdB)', ...
    output.options.algorithm, cpsnr(output.estimate,img)));
colorbar

subplot(224)
fftshow(output.estimate)
title('FFT')

res = real(ifft2(fft2(output.estimate) .* H)) - data;
fprintf('  residuals norm: %.2f/%.2f\n', sqrt(mean(res(:).^2)), noise_std);

%%
% Plot cost functions
figure(2)
if isfield(output,'pd_gap')
    subplot(211)
    loglog(output.iteration,output.pd_gap/(numel(data)*max(data(:))))
    title('Primal dual gap / (npx*max(data))')
end

if isfield(output,'cost')
  subplot(212)
  loglog(output.iteration', sum(output.cost,2));   
  xlabel('Iterations')
  ylabel('Cost function')
  title('Convergence')  
  axis tight
  grid on
end
