% Run all examples

for k = 1:2
    scriptname = sprintf('example%d.m', k);
    try
        run(scriptname);
    catch
        warn('Script %s failed.', scriptname);
    end
end
