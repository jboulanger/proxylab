% Denoising
%
%  argmin_x 1/2 |x-y|^2 + |Dx|
%
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)
disp('Denoising example');
clear all; close all;
setup_proxylab_toolbox();
use_gpu = true;
%% Create the problem
img = 255 * generate_test_image('wavy',256,100); 
%img = generate_test_image('cameraman',256,500); 
noise_std = 0.1 * max(img(:)); % noise level
data = img + noise_std * randn(size(img));
data = to_cuda(data, use_gpu);

%% Restoration

% define the cost function to optimize
npx = numel(img);
lambda = 0.5; % regularization parameter
cost(1) = create_cost_term(create_l2norm_fun(1,data));
% try here other regularization terms (tv, l2-laplacian,schatten-patch)
cost(2) = create_regularization_term(lambda,'tv', data);
%cost(2) = create_regularization_term(lambda,'schatten-hessian', data);
L = compute_multi_operator_norm(cost, randn(size(img)), 20);
opts = [];
opts.max_iter = 1000;
opts.TolGap = 1e-3;
opts.observer = @progressbar_cost;
opts.record = 17;
%opts.use_gap = 0;
opts.init = data;
opts.bounds = [0,256];
opts.normL = 2;
opts.rho = 0.9;
opts.sigma = 10;
opts.tau = 1/(opts.normL*opts.sigma);

output = pdhg(cost, opts);

fprintf('finished in %f seconds (%.2fdB).\n', ...
    max(output.elapsed_time), cpsnr(output.estimate,img));

% Display the result
clf
subplot(221)
imshow(img,[])
title('Original image');
colorbar

subplot(222)
imshow(data,[])

title(sprintf('Data (%.2fdB)', cpsnr(data,img,255)));
colorbar

subplot(223)
imshow(output.estimate,[])
title(sprintf('Denoised (%s) (%.2fdB)', output.options.algorithm, cpsnr(output.estimate,img,255)));
colorbar

%% Plot cost functions
if isfield(output,'pd_gap') || isfield(output,'cost')
    figure(2)
    if isfield(output,'pd_gap')
        subplot(211)
        loglog(output.iteration, output.pd_gap/(numel(data)*max(data(:))))
        title('Primal dual gap / (npx* max(data))')
    end

    if isfield(output,'cost')
        subplot(212)
        loglog(output.iteration, sum(output.cost,2));hold on;   
        xlabel('Iterations')
        ylabel('Cost function')
        title('Convergence')  
        axis tight
        grid on
    end
end