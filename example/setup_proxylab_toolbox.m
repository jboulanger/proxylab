function setup_proxylab_toolbox()
if ~exist('pdhg.m','file')
    addpath('../cost')
    addpath('../function')
    addpath('../operator')
    addpath('../algorithm')
    addpath('../observer')
    addpath('../util')
end
