% Deconvolution of 3D test image
%
%  argmin_x 1/2 |Hx-y|^2 + |Hessian(x)|_*
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

clear all; close all;
setup_proxylab_toolbox();
use_gpu = true;
%% create the deconvolution problem
disp('3D Deconvolution example');
imsize = [64, 64, 32];
pxsize = [0.1, 0.1, 0.2];% micron
%NA = 1.33;
NA = 1;
wavelength = 0.52;% micron
refractive_index = 1.33;
noise_std = 0.001; % noise level
img = generate_test_image('fibers',imsize);
C = [0;0;0;0.01;0;0;0;-0.01];
H = generate_otf3d(imsize, pxsize, NA, wavelength, refractive_index, C);
data = real(ifftn(fftn(img) .* H)) + noise_std * randn(size(img));
figure(1), clf
subplot(221), imshow3(img,[],'mip'), title('Original image')
subplot(222), imshow3(data,[],'mip'), title('Data image')
subplot(223), imshow3(log(abs(0.1+fftshift(H)))), title('OTF (log scale)')
subplot(224), imshow3(sqrt(abs(otf2psf(H)))), title('PSF (gamma=2)')
data = to_cuda(data, use_gpu);
H = to_cuda(H, use_gpu);

%% define the cost function to optimize
disp(' starting reconstruction...')
lambda = 0.00001; % regularization parameter
cost(1) = create_cost_term(create_l2norm_fun(1,data), create_convolution_op(H,'spectral',true));
cost(2) = create_regularization_term(lambda, 'tv', data);
clear opts;
L = compute_multi_operator_norm(cost, randn(size(img)), 20);
opts = [];
opts.init = data;
opts.max_iter = 10000;
opts.record = 20;
opts.observer = @progressbar_cost;
opts.rho = 1;
opts.sigma = 0.1 ;
opts.tau = 1 / (opts.sigma*L);
opts.bounds = [0, 1];

output = pdhg(cost, opts);
fprintf(' %s finished in %f seconds (%.2fdB).\n', output.options.algorithm, max(output.elapsed_time), cpsnr(output.estimate, img));
% Display the result
figure(2)
subplot(221), imshow3(img,[],'mip'), title('Original image')
subplot(222), imshow3(data,[],'mip'), title(sprintf('Data (%.2fdB)', cpsnr(data,img)))
subplot(223), imshow3(output.estimate,[],'mip'), title(sprintf('Deconvolved (%s) (%.2fdB)', output.options.algorithm, cpsnr(output.estimate, img)))

% Plot cost functions
if isfield(output,'cost') && isfield(output,'cost')
    subplot(224)      
    %x = output.elapsed_time';
    x = (1:size(output.cost,1))';
    y = sum(output.cost, 2);
    p = polyfit(log10(x(10:end)), log10(y(10:end)),1);
    f = 10.^polyval(p, log10(x));
    loglog(x, y, x, f);
    xlabel('Time [s]')
    ylabel('Cost function')
    title(sprintf('Convergence (slope:%f)', p(1)))
    axis tight
    axis square
    grid on
end

%% Compare with Richardson-Lucy
figure(3);
opts.max_iter = 1000;
outputrl = richardson_lucy(cost,opts);
fprintf(' %s finished in %f seconds (%.2fdB).\n', outputrl.options.algorithm, max(outputrl.elapsed_time), cpsnr(outputrl.estimate, img));

subplot(221), imshow3(img,[],'mip'), title('Original image'); colorbar
subplot(222), imshow3(data,[],'mip'), title(sprintf('Data (%.2fdB)', cpsnr(data,img))); colorbar
subplot(223), imshow3(output.estimate,[],'mip'), colorbar;
title(sprintf('Deconvolved (%s) (%.2fdB)', output.options.algorithm, cpsnr(output.estimate, img)));
subplot(224), imshow3(outputrl.estimate,[],'mip'), colorbar;
title(sprintf('Deconvolved (%s) (%.2fdB)', outputrl.options.algorithm, cpsnr(outputrl.estimate, img)));


