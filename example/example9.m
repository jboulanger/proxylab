% Deconvolution of 3D test image with mixed noised deconvolution example
%
% Variational model:
% J(u,v) = l2(v-f) + lambda1 DKL(u,v) + lambda2 TV(u)
%

clear all; close all;
setup_proxylab_toolbox();
use_gpu = false;
%% create the deconvolution problem
disp('3D Deconvolution example');
imsize = [64, 64, 16];
pxsize = [0.1, 0.1, 0.2];% micron
%NA = 1.33;
NA = 1;
wavelength = 0.52;% micron
refractive_index = 1.33;
noise_std = 0.5; % noise level
intensity_scaling = 10000;
%img = 255 * generate_test_image('fibers',imsize);
img = intensity_scaling * generate_test_image('fibers',imsize);
C = [0;0;0;0.01;0;0;0;-0.01];
H = generate_otf3d(imsize, pxsize, NA, wavelength, refractive_index, C);
data = poissrnd(real(ifftn(fftn(img) .* H))) + noise_std * randn(size(img));
%data = real(ifftn(fftn(img) .* H)) + 0 * randn(size(img));

figure(1), clf
subplot(221), imshow3(img,[],'mip'), title('Original image'), colorbar
subplot(222), imshow3(data,[],'mip'), title('Data image'), colorbar
subplot(223), imshow3(log(abs(0.1+fftshift(H)))), title('OTF (log scale)'), colorbar
subplot(224), imshow3(sqrt(abs(otf2psf(H)))), title('PSF (gamma=2)'), colorbar
drawnow

data = to_cuda(data, use_gpu);
H = to_cuda(H, use_gpu);

disp(' starting reconstruction...')
lambda1 = 1; % Gaussian fidelity parameter
lambda2 = 50; % KL fidelity parameter
 
% first term: l2(v-f) = l2([0,1][u;v] - f)
T1 = create_stacked_op({create_identity_op(0), create_identity_op(1)});
cost(1) = create_cost_term(create_l2norm_fun(lambda1,data), T1);

% second term: DKL(Hu,v) = DKL([H,0;0,1][u;v])
T2 = create_stacked_op({create_convolution_op(H,'spectral',true), create_identity_op(0); create_identity_op(0), create_identity_op(1)});
cost(2) = create_cost_term(create_kullback_leibler_uv_fun(lambda2), T2);

% third term: TV(u) = l1([Dx; Dy; Dz] [u;v])
Dx = [-1, 1, 0];
Dy = [-1; 1; 0];
Dz = reshape(Dx,[1, 1, 3]);
T3 =  create_stacked_op({create_convolution_op(Dx);...
                         create_convolution_op(Dy);...
                         create_convolution_op(Dz)});
%cost(3) = create_cost_term(create_l1norm_fun(0.0001), T3);
cost(3) = create_cost_term(create_l1norm_fun(0.01), T3);

opts = [];
opts.init = repmat(data,[1 1 1 2]);% need to initialize with proper dimensions
%opts.init = zeros(size(opts.init));
opts.max_iter = 1000;
opts.observer = @progressbar_cost;
opts.record = 30;

%disp('Computing operator norm...')
%f0 = zeros([size(data),2]);
%f0(:,:,:,1) = randn(size(data));
%f0(:,:,:,2) = randn(size(data));
%L = compute_multi_operator_norm(cost,f0);
%L

opts.rho = 1;
opts.tau = 2;
opts.sigma = 1;
%opts.sigma = 1/(opts.tau*L)
%opts.sigma = 0.5/(opts.tau*L)

opts.bounds = [eps, Inf];
%opts.observer = @observer_fft;
output = pdhg(cost, opts);

estimate_u = squeeze(gather(output.estimate(:,:,:,1)));
estimate_v = squeeze(gather(output.estimate(:,:,:,2)));

%fprintf(' %s finished in %f seconds (%.2fdB).\n', output.options.algorithm, max(output.elapsed_time), cpsnr(output.estimate, img));
% Display the result
figure(2)
subplot(221), imshow3(img,[],'mip'), title('Original image'), colorbar
subplot(222), imshow3(data,[],'mip'), title(sprintf('Data (%.2fdB)', cpsnr(data,img))), colorbar
subplot(223), imshow3(estimate_u,[],'mip'), title(sprintf('Deconvolved (%s) (%.2fdB)', output.options.algorithm, cpsnr(estimate_u, img))), colorbar

% Plot cost functions
if isfield(output,'cost') && isfield(output,'cost')
    subplot(224)      
    %x = output.elapsed_time';
    x = (1:size(output.cost,1))';
    y = sum(output.cost, 2);
    p = polyfit(log10(x(10:end)), log10(y(10:end)),1);
    f = 10.^polyval(p, log10(x));
    loglog(x, y, x, f);
    xlabel('Time [s]')
    ylabel('Cost function')
    title(sprintf('Convergence (slope:%f)', p(1)))
    axis tight
    axis square
    grid on
end
drawnow

%%% Compare with Richardson-Lucy
%figure(3);
%opts.max_iter = 100;
%outputrl = richardson_lucy(cost,opts);
%
%estimate_rl_u = squeeze(gather(outputrl.estimate(:,:,:,1)));
%estimate_rl_v = squeeze(gather(outputrl.estimate(:,:,:,2)));
%fprintf(' %s finished in %f seconds (%.2fdB).\n', outputrl.options.algorithm, max(outputrl.elapsed_time), cpsnr(estimate_rl_u, img));
%
%subplot(221), imshow3(img,[],'mip'), title('Original image'), colorbar
%subplot(222), imshow3(data,[],'mip'), title(sprintf('Data (%.2fdB)', cpsnr(data,img))), colorbar
%subplot(223), imshow3(estimate_u,[],'mip'), title(sprintf('Deconvolved (%s) (%.2fdB)', output.options.algorithm, cpsnr(estimate_u, img))); colorbar;
%subplot(224), imshow3(estimate_rl_u,[],'mip'), title(sprintf('Deconvolved (%s) (%.2fdB)', outputrl.options.algorithm, cpsnr(estimate_rl_u, img))); colorbar;


