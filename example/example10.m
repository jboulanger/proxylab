% $|Ax-b|_2^2 + lambda * |Bx|_1$
% with A and B random matrices
%
% Use to test the condition on sigma and tau in PDHG
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

clear all; close all;
setup_proxylab_toolbox();
%%
clear all;
disp('Lasso')
n = 10;
A = randn(10,n);
B = randn(10,n);
x = rand(n,1);
b = A * x;
b = b + 10 * rand * randn(size(b));

% create the cost functions
lambda = 0.1*rand;
cost(1) = create_cost_term(create_l2norm_fun(1, b),   create_matrix_op(A));
cost(2) = create_cost_term(create_l1norm_fun(lambda), create_matrix_op(B));
%
% Using the PDHG algorithm (multi-term chambolle-pock with no smooth term)
opts = [];
opts.max_iter = 10000;
opts.record = 1;
%opts.rho = 2;
%opts.tau = .01;
%opts.sigma = 1;
output = pdhg(cost, opts);
fprintf('  sigma = %f\n', output.options.sigma)
fprintf(1, '  elapsed time %.2fs\n', output.elapsed_time(end));

% Display cost functions
loglog(sum(output.cost,2));
xlabel('Iterations')
ylabel('Cost')
axis tight
