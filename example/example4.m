% Structured Illumination microscopy example
%
% In structured illumination the sample is modulated by an illumination
% pattern and blurred by the optical transfer of the microscope.  High
% resolution image can be obtained by restoring the data acquired with
% various modulation patterns.
%
% This example incorporate a realistic Poisson-Gaussian noise model and
% demonstrate how to manually interact with the minimization procedure.
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

disp('Structured illumination example');
clear all; close all;
setup_proxylab_toolbox();
use_gpu = true;
%% Create a test dataset
period = 4.75; % modulation period (before zoom)
cutoff = 4; % cutoff frequency in pixels
zoom = 2; % zoom factor (1 or 2)
gain = 0.1;% gain of the sensor (max(obj)==1 => max photon==100)
sigma = 0; % readout noise
offset = 0; % camera offset

obj = 255 * generate_test_image('fibers',256); % generate a test image
p = generate_sim_parameters(period,3,3); % generate SIM parameters
m = generate_sim_modulation(size(obj), p); % generate a set of modulation
otf = generate_otf(size(obj,1), cutoff); % compute an OTF
data = generate_sim_data(obj,m,otf,zoom,gain,sigma,offset); % generate SIM data
subplot(121), imshow(append_tiles(data,3,3),[]);
wide_field = apply_resample(mean(data,3), zoom, 2, 'spectral');
subplot(122), imshow(wide_field,[]);

data = to_cuda(data,use_gpu);
otf = to_cuda(otf,use_gpu);
m = to_cuda(m,use_gpu);
wide_field = to_cuda(wide_field,use_gpu);
%% Reconstruct the dataset
disp('Reconstruction');
T1 = create_structured_illumination_op(otf, m, 1/zoom, 'spectral', true);
f1 = create_kullback_leibler_fun(400, data, gain, offset, sigma);
cost(1) = create_cost_term(f1, T1);
% try here other regularization terms (tv, l2-laplacian, schatten-patch, nltv, etc)
cost(2) = create_regularization_term(0.1, 'schatten-hessian', ndims(data));

clear opts;
opts.max_iter = 50;
opts.observer = @progressbar;
opts.record = 1; % change to 1, to graph the optimization
opts.tau = 50;
opts.sigma = 0.1;
opts.rho = 1.;
opts.init = wide_field;
opts.bounds = [0, Inf];
tic;
output = pdhg(cost, opts);

fprintf('%s (%s(%s) + %g x %s(%s)) : %.2fdB\n', output.options.algorithm, ...
    cost(1).function.name, cost(1).operator.name, ...
    cost(2).function.lambda, cost(2).function.name, cost(2).operator.name,...
    cpsnr(output.estimate,obj));
toc;

res = T1.apply(output.estimate) - data;
fprintf('res norm %f\n', std(res(:)));

% Display the result
clf;
subplot(231)
imshow(obj,[])
title('Original image')
%
subplot(232)
imshow(wide_field ,[])
title(sprintf('Data (%.2fdB)', cpsnr(wide_field,obj)))
subplot(235), fftshow(wide_field)

subplot(233)
imshow(output.estimate,[])
title(sprintf('Reconstruction (%s) (%.2fdB)', output.options.algorithm, cpsnr(output.estimate,obj)))
subplot(236), fftshow(output.estimate)

subplot(234),cla
if isfield(output,'cost') && isfield(output,'cost')
    % Plot cost functions
    loglog(sum(output.cost,2));
    xlabel('Iterations')
    ylabel('Cost function')
    title('Convergence')
    axis tight
    axis square
    grid on
else
    % otherwise display FFT of the original object
    fftshow(obj);
end

