% Mixed noised deconvolution example
%
% variational model
% J(u,v) = l2(v-f) + lambda1 DKL(u,v) + lambda2 TV(u)
%
% https://arxiv.org/pdf/1611.00690.pdf
%
% same as example12 but only one parameter here

clear all; close all;
setup_proxylab_toolbox();
rng(1)
use_gpu = 0;
%% create the deconvolution problem
disp('Mixed noised deconvolution example');
N = 64;
img = 1000 * (generate_test_image('steps',N,8));
%img = 1000 * (generate_test_image('fibers',N));
H = generate_otf(N,3);
noise_std = 10;
data = poissrnd(real(ifft2(fft2(img) .* H))) + noise_std * randn(size(img));

figure(1);clf;
subplot(221), imshow(img,[]), title('Original image');
subplot(222), imshow(data,[]), title('Data image');
subplot(223), imshow(log(0.1+fftshift(H)),[]), title('OTF (log scale)');
subplot(224), imshow(sqrt(otf2psf(H)),[]), title('PSF (gamma=2)');
drawnow
%%  copy arrays to the GPU
data = to_cuda(data,use_gpu);
H = to_cuda(H,use_gpu);

%% Define options
opts = [];
%opts.init = repmat(data,[1 1 2]);% need to initialize with proper dimensions
opts.init = zeros(N,N,2);
opts.max_iter = 10000;
opts.record = false;
opts.sigma = 0.1;
opts.tau = 0.25;
opts.rho = 0.9;
opts.bounds = [0, Inf];
opts.data = data;
opts.H = H;
%opts.observer = @progressbar;

%% Optimizing regularization with lineseach
disp('Optimizing regularization with lineseach')
lambda = logspace(0,4,100);
tic
c = zeros(2,length(lambda));
c_sum = zeros(size(lambda));
fid_terms = zeros(3,length(lambda));
mse = zeros(1,length(lambda));
allu = zeros(N,N,length(lambda));
allv = zeros(N,N,length(lambda));
parfor i=1:length(lambda)
    [c_total, c_sep, outpt, ft] = discrepancy(lambda(i),opts,noise_std);
    u = outpt.estimate(:,:,1);
    mse(i) = mean((u(:)-img(:)).^2);
    c(:,i) = c_sep;
    c_sum2(i) = sum((c_sep-[0.5;0.5]).^2);
    c_sum(i) = (sum(c_sep)-1).^2;
    fid_terms(:,i) = ft;
    allu(:,:,i) = u;
    allv(:,:,i) = outpt.estimate(:,:,2);
end
toc
[vmse,kmse] = min(mse);  
[~,kcsep] = min(c_sum2); 
disp(table({'MSE';'Discrepancy'},[vmse; mse(kcsep)],lambda([kmse;kcsep])','VariableName',{'Criterion','MSE','Lambda'}));
figure(2),clf
loglog(lambda,c_sum2,lambda,mse,lambda,sum(c),'linewidth',2);
lgd1 = legend('(c1-1/2)^2+(c2-1/2)^2','mse','c1+c2');
set(lgd1,'FontSize',7);
xlabel('Regularization parameter');
ylabel('Cost function')

%% Optimize the regularization parameter using fminsearch
tic;
disp('Optimizing regularization parameter')
lambda0 = lambda(kcsep); % initialize from the minimum of the (c1-0.5)^2+(c2-0.5)^2
discrepancy_fun = @(l) discrepancy(10^l,opts,noise_std);
lambdastar = 10^fminsearch(discrepancy_fun,log10(lambda0));
[cstar c_sep output] = discrepancy(lambdastar,opts,noise_std);
ustar = output.estimate(:,:,1);
msestar = mse2(img,ustar);
table({'MSE';'Linesearch';'fminseach'},[vmse; mse(kcsep); msestar],[lambda([kmse;kcsep])'; lambdastar],'VariableName',{'Criterion','MSE','Lambda'})
toc
%% Graphs
figure(2),clf
loglog(lambda,c_sum2,lambda,mse,lambda,sum(c),'linewidth',2);
hold on;
loglog(lambdastar,cstar,'r+','MarkerSize',10);
loglog(lambda,ones(size(lambda)),'linewidth',2)
hold off;
lgd1 = legend('(c1-1/2)^2+(c2-1/2)^2','mse','c1+c2','optimized','1');
set(lgd1,'FontSize',7);
xlabel('Regularization parameter');
ylabel('Cost function')

%% Recompute the discrepancy directly
D = zeros(2,numel(lambda));
E = zeros(1,numel(lambda));
for k =1:numel(lambda)
    D(:,k) = discrepancy2(H,data,allu(:,:,k),allv(:,:,k),noise_std);
    E(k) = mse2(img,allu(:,:,k));
end
figure(5);
loglog(lambda,D(1,:),lambda,D(2,:),lambda,E,lambda,0.5*ones(size(lambda)),lambda,(sum(D)-1).^2);
legend('DKL','L2','MSE','0.5','(c1+c2-1)^2') 
title('Discrepancy')
xlabel('Regularization parameter')
%% test discrepancy with simulated vectors
lbd = linspace(0,1000);
u0 = ones(10000,1)*lbd;    % intensity
v0 = poissrnd(u0);        % Poisson 
f0 = v0 + randn(size(v0));% Gaussian
dkl = mean(u0 - v0 + v0.* log(v0./u0));
l2 = mean((f0-v0).^2)/2;
figure(6)
plot(lbd,dkl,lbd,l2)
axis([0 lbd(end) 0 1]);
xlabel('Intenisty (count)');
ylabel('Discrepancy');
legend('DKL','L2')
%%  show results
[~,~,output] = discrepancy(lambdastar,opts,noise_std);
figure(3), plot_results(img,data,H,output);

[~,~,output] = discrepancy(lambda(kmse),opts,noise_std);
figure(4), plot_results(img,data,H,output);
%% 
function e = mse2(u0,u)
% Mean square error
e = mean((u0(:)-u(:)).^2);
end

function d = discrepancy2(L,f,u,v,sigma)
% Compute discrepancy from the estimates and the data
Lu = real(ifftn(fftn(u) .* L));
d = [   mean(Lu(:) - v(:) + v(:) .* log(v(:)./Lu(:))), mean( (f(:) - v(:)).^2 )./(2*sigma^2)];
end

function [c c_sep output fid_terms] = discrepancy(lambda,opts,noise_std)
  
  lambda1 = lambda/(noise_std^2);
  lambda2 = lambda;
 
  % first term: l2(v-f) = l2([0,1;0 0][u;v] - [f;0])
  T1 = create_stacked_op({create_identity_op(0), create_identity_op(1);...
      create_identity_op(0), create_identity_op(0)});
  datal2 = zeros([size(opts.data),2]);
  datal2(:,:,1) = opts.data;
  cost(1) = create_cost_term(create_l2norm_fun(lambda1,datal2), T1);

  % second term: DKL(Hu,v) = DKL([H,0;0,1][u;v])
  T2 = create_stacked_op({create_convolution_op(opts.H,'spectral',true), create_identity_op(0); create_identity_op(0), create_identity_op(1)});
  cost(2) = create_cost_term(create_kullback_leibler_uv_fun(lambda2), T2);

  % third term: TV(u) = l1([Dx 0;Dy 0] [u;v])
  Dx = [-1, 1, 0];
  Dy = [-1; 1; 0];
  T3 =  create_stacked_op({create_convolution_op(Dx), create_identity_op(0);...
                           create_convolution_op(Dy), create_identity_op(0)});
  cost(3) = create_cost_term(create_l1norm_fun(1), T3);

  output = pdhg(cost, opts);
    
  estimate = output.estimate;
  if ndims(estimate) == 3
    npx = prod(size(estimate(:,:,1)));
  else % ndims == 4
    npx = prod(size(estimate(:,:,:,1)));
  end
 
  if opts.record
    cost1 = output.cost(end,1);
    cost2 = output.cost(end,2);
    cost3 = output.cost(end,3);
  else
    cost1 = cost(1).eval(output.estimate);
    cost2 = cost(2).eval(output.estimate);
    cost3 = cost(3).eval(output.estimate);
  end

  fid_terms = zeros(3,1);
  fid_terms(1) = cost1/(npx*lambda1);
  fid_terms(2) = cost2/(npx*lambda2);
  fid_terms(3) = cost3/npx;

  c1 = cost1/(npx * lambda1 * noise_std^2) ; 
  c2 = cost2/(npx * lambda2) ; 
  c3 = cost3/npx;

  c_sep = [c1; c2];
  %c = sum(c_sep);
  c = (c1 + c2 - 1)^2;
  c = (c1-0.5)^2 + (c2-0.5)^2;
  %c = (c1 + c2 - 0.5)^2;
  %c = c1^2
end

function plot_results(img,data,H,output)
 
  estimate_u = squeeze(gather(output.estimate(:,:,1)));
  estimate_v = squeeze(gather(output.estimate(:,:,2)));

  M = length(img(:));
  rmse = sqrt(sum(sum((estimate_u - img).^2))/M);

  % Display the result
  clf;
  subplot(241)
  imshow(img,[])
  title('Original image');
  colorbar

  subplot(245)
  imshow(data,[])
  title(sprintf('Data f (%.2fdB)', psnr(img,gather(data),255)));
  colorbar

  subplot(242)
  imshow(estimate_u,[])
  title(sprintf('Deconvolved u (%s) (%.2fdB, RMSE=%.2f)', output.options.algorithm, psnr(img,estimate_u,255),rmse));
  colorbar

  subplot(246)
  imshow(estimate_v,[]);
  title('v (data without Gaussian noise)');
  colorbar

  subplot(243)
  imshow(data-estimate_v,[]);
  title('data - v (estimated Gaussian noise)');
  colorbar

  Hu = real(ifft2(fft2(estimate_u) .* H));
  subplot(247)
  imshow(estimate_v - Hu,[]);
  title('v - Hu (estimated Poisson noise)');
  colorbar

  % Plot cost functions
  if isfield(output,'cost') && isfield(output,'cost')
      subplot(248)
      loglog(sum(output.cost,2));hold on;
      xlabel('Iterations')
      ylabel('Cost function')
      title('Convergence')
      axis tight
      grid on
  end

end

