%% LASSO problem with $|Ax-b| + lambda * |B x|$
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

clear all; close all;
setup_proxylab_toolbox();
%%
disp('Lasso')

A = randn(5,10);
x = 1*rand*rand(size(A,2), 1);
b = A * x;
lambda = .0001;
B = 1*rand*randn(2, size(A,2));
fprintf('|A| = %f |B| = %f\n', norm(A), norm(B));

%% create the cost functions
% The data term $|Ax-b|^2$ using a function involving a linear operator
% Here $f(.) = |A.-b|^2$ and $T = I$
cost(1) = create_cost_term(create_l2norm_fun(1,b), create_matrix_op(A));
% The L1 norm term |x|
cost(2) = create_cost_term(create_l1norm_fun(lambda), create_matrix_op(B));

%% Using the PDHG algorithm (multi-term chambolle-pock with no smooth term)
disp('Running PDGH')
clear opts;
opts.max_iter = 200;
opts.record = 1;
output = pdhg(cost, opts);
fprintf(1, '  norm of the error %f\n', norm(output.estimate - x) / norm(x));
fprintf(1, '  elapsed time %.2fs\n', output.elapsed_time(end));
fprintf(1, '  minimum cost %f\n', sum(output.cost(end,:),2));
% Display cost functions
clf
subplot(121)
loglog(sum(output.cost,2),'b','LineWidth',2);
ylabel('Cost')
xlabel('Time [s]')
title('Lasso')
axis tight
axis square
grid on
subplot(122)
loglog(output.elapsed_time,sum(output.cost,2),'b','LineWidth',2);
xlabel('Time [s]')
ylabel('Cost')
title('Lasso')
axis tight
axis square
grid on
