function output = chambolle_pock(cost, options)
%
% output = chambolle_pock(cost, options)
%
%  minimize  cost(1) + cost(2)
%
%  with cost(1) = G(x)
%       cost(2) = F(Kx)
%
%
% Chambolle, A., Pock, T.: A first-order primal–dual algorithm for convex
% problems with applications to imaging. J. Math. Imaging Vis. 40(1),
% 120–145 (2011) http://www.cmapx.polytechnique.fr/preprint/repository/685.pdf
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

options.algorithm = 'chambolle pock';

if length(cost) > 2
    error('Can minimize only the sum of two functions.')
end

if ~isfield(options, 'max_iter')
    options.max_iter = 20;
end

if ~isfield(options, 'theta')
    options.theta = 0.75;
end

if ~isfield(options, 'tau')
    options.tau = .1;
end

if ~isfield(options, 'sigma')
    options.sigma = 1;
end

if ~isfield(options, 'accelerate')
    options.accelerate = 1;
end

if ~isfield(options, 'init')
    % search for a data term in one of the cost terms
    for i = 1:length(cost)
        if isfield(cost(i).function,'data')
            options.init = zeros(size(cost(i).operator.apply_adjoint(cost(i).function.data)));
            break;
        end
    end
end

if ~isfield(options, 'record')
    options.record = 0;
end

if ~isfield(options, 'naff')
    options.naff = 10;
end

% compute prox_{gamma F*}(x) as x - tau * prox_{1/tau F}(y/tau)
% probably if it would be directly implemented it would be faster
if ~isfield(cost(2).function, 'prox_star')
    cost(2).function.prox_star = @(x,gamma) x - gamma * cost(2).function.prox(x / gamma, 1/ gamma);
end

output.options = options;

x0 = options.init;
y = cost(2).operator.apply(x0);
xbar = x0;
tau = options.tau;
sigma = options.sigma;
theta = options.theta;

t0 = tic;

output = record_and_display(cost, options, output, x0,  1, t0, eval_cost(cost,x0));
  
for k = 1:options.max_iter

    y  = cost(2).function.prox_star(y + sigma * cost(2).operator.apply(xbar), sigma);
    x1 = cost(1).function.prox(x0 - tau * cost(2).operator.apply_adjoint(y), tau);
    if options.accelerate == 1
        theta = 1 / sqrt(1 + 0.1 * tau);
        tau = theta * tau;
        sigma = theta * sigma;
    end
    xbar = x1 + theta * ( x1 - x0 );
    x0 = x1;

    output = record_and_display(cost, options, output, x0,  k, t0, eval_cost(cost,x0));
end
