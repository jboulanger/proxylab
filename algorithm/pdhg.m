function output = pdhg(cost, options)
%
% output = pdhg(cost, options)
%
% multi-term (>=2) primal dual hybrid gradient descent as presented in
% (Condat, 2012) assimung no F (lipschitz) and G (smooth).
%
% Input :
%  cost    : array of struct of cost function f(Tx)
%            with field: 'function' for f and 'operator' for T
%
%  options : struct with optional fields
%           rho = 1, tau = 1, sigma = 1/norm, init = T'(data),
%           record = 0, bounds = [-Inf,Inf]
%
%           If options.record == 1; then cost and elspased time are
%           recorded over the iterations.
%
%           options.observer = @(cost, options, output)() a function
%
%           Note that recording the evolution of the cost function and
%           plotting the estimate takes additional time.
%
% Output :
%  output : struct with estimate and elapsed_time
%           If record is true, a field 'cost' is an array of size max_iter
%           x length(cost) with the value of each term over time; and
%           'elapsed_time' is an array of all time stamps over the
%           iterations.
%
% L. Condat, A primal-dual splitting method for convex optimization
% involving Lipschitzian, proximable and linear composite terms," Journal
% of Optimization Theory and Applications, vol. 158, no. 2, pp. 460-479,
% Aug. 2012.  https://link.springer.com/article/10.1007/s10957-012-0245-9
%
%
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

options.algorithm = 'pdhg';

if ~isfield(options, 'max_iter')
    options.max_iter = 500;
end

if ~isfield(options, 'TolGap')
    options.TolGap = 1e-6;
end

if ~isfield(options, 'TolX')
    options.TolX = 1e-6;
end

if ~isfield(options, 'bounds')
    options.bounds = [-Inf, Inf];
end

% search for a data term in one of the cost terms
for i = 1:length(cost)
    if isfield(cost(i).function,'data')
        data = cost(i).function.data;
        if ~isfield(options, 'init')
            options.init = zeros(size(cost(i).operator.apply_adjoint(data)));
        end
        break;
    end
end

if ~isfield(options, 'rho')
    options.rho = 0.9;
end

if ~isfield(options,'tau')
    options.tau = 0.1;
end

if ~isfield(options, 'sigma')
    disp('compute L2')
    x0 = randn(size(options.init),class(options.init));
    L2 = compute_multi_operator_norm(cost, x0,50);
    fprintf('  L2 = %f\n', L2);
    % Theorem 5 5.2 sigma tau |sum_q T_q*Tq| <= 1
    % here we have sigma = 1 / (tau sum |L|^2
    options.sigma = 0.999 / (options.tau * L2);
end

if ~isfield(options, 'record')
    options.record = 0;
end

if ~isfield(options, 'naff')
    options.naff = 10;
end

output.options = options;

% Initialize the variable
x0 = options.init;
y = x0;

v = cell(1,length(cost));
for i = 1:length(cost)
    %v{i} = zeros(size(cost(i).operator.apply(x0)),class(x0));
    v{i} = cost(i).operator.apply(x0);
end

% Determine if the primal-dual gap will be used as stopping criterion:
% if the user explicitly sets options.use_gap = 0, OR
% if record ~= 0 and all cost functions have convex conjugate defined.
if isfield(options,'use_gap') && ~options.use_gap
    use_gap = 0;
else
    use_gap = (options.record ~= 0);
    for i = 1:length(cost)
        if ~isfield(cost(i).function,'conv_conj')
            use_gap = 0;
        end
    end
end

t0 = tic;

sigma = options.sigma;
tau = options.tau;
rho = options.rho;

ui = cell(1,length(cost));
pi = cell(1,length(cost));

pd_gap = Inf;

for k = 1:options.max_iter
   
    delta = 2 * y - x0;
    try
        for i = 1:length(cost)
            ui{i} = v{i} + sigma * cost(i).operator.apply(delta);
            pi{i} = ui{i} - sigma * cost(i).function.prox(ui{i} / sigma, 1 / sigma);
            v{i} = rho * pi{i} + (1 - rho) * v{i};
        end
    catch ME
        if strcmp(ME.identifier, "solve_prox_kl_ut_alt:max_itersReached")
        	warning(ME.message)
            break 
        end  
    end

    y = x0;
    Lsyi = cell(1,length(cost));
    for i = 1:length(cost)
        Lsyi{i} = cost(i).operator.apply_adjoint(v{i});
        y = y - tau * Lsyi{i};
    end

    if options.record && (rem(k, options.record) == 0 || k == options.max_iter)
        primal_cost = eval_cost(cost,x0);
        if use_gap
            dual_cost = eval_dual_cost(cost,v,options.bounds,Lsyi);
            pd_gap = sum(primal_cost) - dual_cost;
        end
    end

    if sum(isnan(y(:))) > 0
        warning("PDHG stopped because the last iteration contains NaNs")
        if options.record && (rem(k, options.record) == 0 || k == options.max_iter)
            output = record_and_display(cost, options, output, x0,  k, t0, primal_cost);
        else
            output = record_and_display(cost, options, output, x0,  k, t0);
        end
        break
    end
    
    if isfield(options, 'bounds')
        y = max(options.bounds(1), min(options.bounds(2), y));
    end

    x1 = options.rho * y + (1 - options.rho) * x0;
  
    break_condition = ((~options.record || ~use_gap) && 2*norm(x0(:)-x1(:))/norm(x0(:)+x1(:)) < options.TolX) ...
        || (use_gap && pd_gap < options.TolGap * max(data(:)) * numel(data));
    
    % If break_condition is satisfied, the last cost and pd_gap will be
    % copied from the previously calculated
    if options.record && (rem(k, options.record) == 0 || k == options.max_iter || break_condition)
        if use_gap
            output = record_and_display(cost, options, output, x1,  k, t0, primal_cost, pd_gap);
        else
            output = record_and_display(cost, options, output, x1,  k, t0, primal_cost);
        end
    else
        output = record_and_display(cost, options, output, x1,  k, t0);
    end
        
    if break_condition
        break 
    end
    
    x0 = x1;
end

