function output = richardson_lucy(cost, options)
% output = richardson_lucy(operator, data, options)
%
% Richardson Lucy algorithm applied to the 1st operator of the cost
% function
%
% Input:
%  cost : a structure with cost.function.data and cost.operator.apply,
%         cost.operator.apply_adjoint
%  options: a structure with max_iter (default 500)
%   
% Output:
%  output: structure with fields 'estimate', 'elapsed_time', 'iterations', 'options'
%
% Jerome Boulanger 2020

options.algorithm = 'richardson_lucy';

if ~isfield(options, 'max_iter')
    options.max_iter = 500;
end

if ~isfield(options, 'record')
    options.record = 0;
end

if ~isfield(options, 'naff')
    options.naff = 10;
end

output.options = options;
f = cost(1).function.data;
t0 = tic;
x0 = f;
for iter = 1:options.max_iter
    x0 = x0 .* cost(1).operator.apply_adjoint(f ./ cost(1).operator.apply(x0));
    output = record_and_display(cost, options, output, x0, iter, t0);
end

