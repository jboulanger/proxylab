function output = ppxa(cost, options)
%
%  output = ppxa(cost, options)
%
% Input
%  cost: array of struct of cost function f(x) with field: func f
%
%  options: struct with optional fields
%           rho=1,tau=1,sigma=1/norm,init=Tstar(data),
%           record=0,bounds=[-Inf,Inf],display
%           options.display is a function with parameters:
%           (k, cost, options, output, x0, x1)
% Output
%  output : struct with estimate, criterion, time
%
%  Patrick L. Combettes and Jean-Christophe Pesquet. Proximal splitting
%  methods in signal processing. In Fixed-point algorithms for inverse
%  problems in science and engineering, volume 49 of Springer Optim. Appl.
%  pages 185-212. Springer, New York, 2011.
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)


options.algorithm = 'ppxa';

if ~isfield(options, 'max_iter')
  options.max_iter = 20;
end

if ~isfield(options, 'rho')
  options.rho = 2;
end

if ~isfield(options, 'gamma')
  options.gamma = 0.05;
end

if ~isfield(options, 'init')
    % search for a data term in one of the cost terms
    for i = 1:length(cost)
        if isfield(cost(i).function,'data')
            options.init = zeros(size(cost(i).function.Aty));
            break;
        end
    end
end

if ~isfield(options, 'record')
  options.record = 1;
end

if ~isfield(options, 'naff')
  options.naff = 1;
end

output.options = options;

% Initialize the variable
x0 = options.init;
u = cell(length(cost));
p = cell(length(cost));
for i = 1:length(cost)
  u{i} = x0;
end

t0 = tic;

output = record_and_display(cost, options, output, x0,  1, t0, eval_cost(cost,x0));

for k = 1:options.max_iter  
  
  for i = 1:length(cost)
    p{i} = cost(i).function.prox(u{i}, options.gamma * length(cost));    
  end
  
  P = p{1};
  for i = 2:length(cost)
      P = P + p{i};
  end
  P = P / length(cost);
  
  for i = 1:length(cost)
      u{i} = u{i} + options.rho * (2 * P - x0 - p{i});
  end
  x1 = x0 + options.rho * (P - x0);
   
  x0 = x1;
  
  output = record_and_display(cost, options, output, x0,  k, t0, eval_cost(cost,x0));
  
end