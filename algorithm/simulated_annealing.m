function output = simulated_annealing(cost, x0, options)
  %
  % output =  simulated_annealing(cost, options)
  %
  % Input :
  %  cost    : cost function cost.function = @(x)();
  %
  %  options : struct with optional fields
  %             max_iter, Tmin, alpha, accept=@(x,y,t)exp((x-y)/t)
  %             lower_bound, upper_bound
  %
  % Output:
  %  output : struct with estimate and elapsed_time
  %
  %
  % This approach is suited for non-convex problems.
  %
  % Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)
  %

  options.algorithm = 'simulated_annealing';

  if ~isfield(options, 'Tmin')
    options.Tmin = 1e-6;
  end

  if ~isfield(options, 'alpha')
    options.alpha = .9;
  end

  if ~isfield(options, 'accept')
    options.accept = @(x,y,t) exp((x-y)/t);
  end

  if ~isfield(options, 'neighbor')
    options.neighbor = @(x,o,t) neighbor(x,o,t);
  end

  if ~isfield(options, 'lower_bound')
    options.lower_bound = -Inf*ones(size(x0));
  end

  if ~isfield(options, 'upper_bound')
    options.upper_bound =  Inf*ones(size(x0));
  end

  if ~isfield(options, 'record')
    options.record = 0;
  end

  if ~isfield(options, 'naff')
    options.naff = 10;
  end

  if ~isfield(options, 'max_iter')
    options.max_iter = 1000;
  end

  output.options = options;

  t0 = tic;
  f0 = cost.function(x0);
  f2 = f0;
  x2 = x0;
  output = record_and_display(cost, options, output, x0, 1, t0);
  T = 1.0;
  iter = 0;
  while T > options.Tmin && iter < options.max_iter
    iter = iter + 1;
    for i = 1:length(x0)
      x1 = options.neighbor(x0, options, T);
      f1 = cost.function(x1);
      if options.accept(f0,f1,T) > rand
	x0 = x1;
	f0 = f1;
      end
      if f1 < f2
	f2 = f1;
	x2 = x1;
      end
      %show(cost, x0, x1, x2)
    end
    T = T * options.alpha;
    output = record_and_display(cost, options, output, x2, iter, t0);
  end
  output.estimate = x2;
end


function show(cost, x0, x1, x2)
  if length(x0)==1
    plot(-10:.1:10, cost.function(-10:.1:10));
    hold on
    plot(x0, cost.function(x0),'mo','MarkerSize',5);
    plot(x1, cost.function(x1),'ro','MarkerSize',5);
    plot(x2, cost.function(x2),'go','MarkerSize',15);
    hold off;
  else
     [x,y] = meshgrid(-10:.1:10,-10:.1:10);
     imagesc(-10:.1:10,-10:.1:10,20*sin(x).*sin(y)+x.*x+y.*y,[]);
     hold on
      plot(x0(1),x0(2),'mo','MarkerSize',10);

      plot(x1(1),x1(2),'ro','MarkerSize',10);
      plot(x2(1),x2(2),'go','MarkerSize',10);
      hold off
      axis([-10 10 -10 10])
  end
    title(sprintf('simulated annealing x=%.2f',x2))
    drawnow
end

function x = neighbor(x, options, T)
  i = ceil(rand * numel(x));
  x(i) = x(i) + randn * T * (options.upper_bound(i) - options.lower_bound(i));
  %x = x + randn(size(x)) .* T .* (options.upper_bound - options.lower_bound);
  x = max(options.lower_bound, min(options.upper_bound, x));
end
