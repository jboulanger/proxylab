function output = fista(cost, options)
%
%  output = fista(cost, options)
%
%  Input:
%  cost : vector of 2 cost terms with the fields 'function' and 'operator'
%         The first cost term defines cost(1).function.grad
%         The second cost term defines cost(2).function.prox
%
%  Amir Beck and Marc Teboulle. A fast iterative shrinkage-thresholding
%  algorithm for linear inverse problems. SIAM J. Imaging Sci. , 2(1):183
%  202, 2009
%  http://mechroom.technion.ac.il/~becka/papers/71654.pdf
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

options.algorithm = 'fista';

if length(cost)~= 2
    error('fista need a cost function with 2 terms.')
end

if ~isfield(cost(1).function, 'grad')
    error('fista need a first term with a gradient (cost(1).function.grad).')
end

if ~isfield(cost(2).function, 'prox')
    error('fista need a second term with a prox (cost(2).function.prox).')
end

if ~isfield(options, 'max_iter')
    options.max_iter = 20;
end

if ~isfield(options, 'init')
    % search for a data term in one of the cost terms
    for i = 1:length(cost)
        if isfield(cost(i).function,'data')
            options.init = zeros(size(cost(i).function.Aty));
            break;
        end
    end
end

if ~isfield(options, 'record')
    options.record = 1;
end

if ~isfield(options, 'naff')
    options.naff = 1;
end

output.options = options;

x0 = options.init;
y = x0;

t0 = 1;

tstart = tic;

output = record_and_display(cost, options, output, x0,  1, tstart, eval_cost(cost,x0));

% Lipschitz constant defined by |\nabla f(x)- \nabla f(y)| < L |x - y|
% If f(x) = |Ax-b|²/2 then L = max(svd(A'A))
L = cost(1).function.L;

for k = 1:options.max_iter
    
    x1 = cost(2).function.prox(y - cost(1).function.grad(y) / L, L);
    
    t1 = (1 + sqrt(1 + 4 * t0^2)) / 2;
    rho = (t0 - 1) / t1;
    y = x0 + rho * (x1 - x0);
    
    t0 = t1;
    x0 = x1;

    output = record_and_display(cost, options, output, x0,  k, tstart, eval_cost(cost,x0));
    
end
