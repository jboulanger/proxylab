function p = cpsnr(img, ref, peak, debias)
%
% p = cpsnr(ref, img, peak)
%
% Compute the PSNR between ig and ref copying the images from GPU to CPU if
% needed and debiaising img if debias is set to true.
%
% Jerome Boulanger (2020)
%

ref = double(to_cpu(ref));
img = double(to_cpu(img));

if nargin < 4
    debias = false;
end

if nargin < 3
    peak = [];
end

if isempty(peak)
    peak = max(ref(:));
else
    peak = to_cpu(peak);
end

if debias 
     p = polyfit(ref,img,1);
     img = (img - p(2)) / p(1);
end

mse = mean((img(:)-ref(:)).^2);
p = 10 * log10(peak.^2 / mse);
