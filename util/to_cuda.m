function array = to_cuda(array, use_gpu)
% array = to_cuda(array, use_gpu)
% array = to_cuda(array)
% 
% Transfer the data to the GPU if GPU exists and not in octave
%
% Jerome Boulanger (2020) 

if nargin < 2
    use_gpu = true;
end

if use_gpu 
    if exist('OCTAVE_VERSION', 'builtin') == 0
        if gpuDeviceCount > 0
            array = gpuArray(array);
        end
    end
end

