function array = to_cpu(array)
% array = to_cpu(array)
% 
% Transfer the data back from GPU if needed
%
% Jerome Boulanger (2020) 


if exist('OCTAVE_VERSION', 'builtin') == 0
    if isa(array, 'gpuArray')
        array = gather(array);
    end
end


