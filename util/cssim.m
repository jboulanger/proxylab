function val = cssim(img, ref, debias)
%
% p = cssim(ref, img, peak, debias)
%
% Compute the SSIM between img and ref copying the images from GPU to CPU if
% needed and debiaising img if debias is set to true.
%
% Jerome Boulanger (2021)
%

ref = double(to_cpu(ref));
img = double(to_cpu(img));

if nargin < 4
    debias = false;
end

if debias 
     p = polyfit(ref,img,1);
     img = (img - p(2)) / p(1);
end
val = ssim(img, ref);
