function u = generate_test_image(type,dims,varargin)
% u = generate_test_image(type,dims,varargin)
%
% Generate a random test image
%
% Input
%  type : 'wavy', 'fibers', 'beads', 'tissue', 'steps'
%  dims    : size of the image
%
% Output
%  u  : array of size dims normalize in 0,1 with various patterns defined by type
%
% for the wavy pattern, the next parameters are the
%    bandwidth/smoothness (default 1)
%    frequency            (default 8)
% for the fibers pattern, the next parameters are
%    number of fibers     (default 30)
%    length of the fibers (default 2 max(dims))
%    smoothness/curvature (default 2)
%    smoothness/bluriness (default 1)
%
% Jerome Boulanger (2018)

if isscalar(dims)
    dims = [dims,dims,1];
end

if numel(dims) == 2
    dims = [dims(:); 1]';
end

if strcmpi(type,'wavy')
    if (nargin < 3)
        a = 100;
    else
        a = varargin{1};
    end
    if (nargin < 4)
        f = 8;
    else
        f = varargin{2};
    end
    u = randn(dims);
    [kx,ky,kz] = freqgrid(dims);
    if dims(3) == 1
        g = exp(-0.1*a.^2*(kx.^2+ky.^2));
    else
        g = exp(-0.1*a.^2*(kx.^2+ky.^2+kz.^2));
    end
    u = normalize(real(ifftn(u .* g)),-1,1);
    u = normalize(sin(2 * pi * f * u ), 0, 1);
elseif strcmpi(type,'fibers')
    if (nargin < 3)
        m = 50; % number of fibers
    else
        m = varargin{1};
    end
    if (nargin < 4)
        l = 2 * max(dims);
    else
        l = varargin{2};
    end
    if (nargin < 5)
        s1 = 20;
    else
        s1 = varargin{3};
    end
    if (nargin < 6)
        bg = 0.1;
    else
        bg = varargin{4};
    end
    % init filters
    h1 = fspecial('gaussian',[3*s1,1],s1);
    % create fibers as smoothed random walks
    dx = imfilter(randn(l,m), h1) * dims(2);
    dy = imfilter(randn(l,m), h1) * dims(1);
    dz = imfilter(randn(l,m), h1) * dims(3);
    v = sqrt(dx.^2+dy.^2+dz.^2);
    x = repmat(dims(2) * (0.1+0.8*rand(1,m)),[l, 1]) + cumsum(dx./v,1);
    y = repmat(dims(1) * (0.1+0.8*rand(1,m)),[l, 1]) + cumsum(dy./v,1);
    z = repmat(dims(3) * (0.1+0.8*rand(1,m)),[l, 1]) + cumsum(dz./v,1);
    idx = find(x>=1 & x<=dims(2) & y>=1 & y<=dims(1) & z >=0 & z <= dims(3));
    u = zeros(dims);
    for k = 1:numel(idx)
        xi = max(1,floor(x(k)-3)):min(dims(2),ceil(x(k)+3));
        yi = max(1,floor(y(k)-3)):min(dims(1),ceil(y(k)+3));
        zi = max(1,floor(z(k)-3)):min(dims(3),ceil(z(k)+3));
        [xs,ys,zs] = meshgrid(xi,yi,zi);
        u(sub2ind(dims,xs,ys,zs)) = u(sub2ind(dims,xs,ys,zs)) + exp(-2*((xs-x(k)).^2+(ys-y(k)).^2+(zs-z(k)).^2));
    end
    % add a background (~scattering)
    if bg > 0
        [kx,ky,kz] = freqgrid(dims);
        if dims(3) == 1
            g = exp(-10000*(kx.^2+ky.^2));
        else
            g = exp(-10000*(kx.^2+ky.^2+kz.^2));
        end
        b = normalize(real(ifftn(fftn(u) .* g)),0,1);
        u = normalize(b .* (bg + u), 0, 1);
        u = u.^0.75;
    end
elseif strcmpi(type,'steps')
    if nargin < 3
        nsteps = 5;
    else
        nsteps = varargin{1};
    end
    [x,y,z] = meshgrid(0:dims(2)-1,1:dims(1),1:dims(3));
    u = floor(nsteps * x/(dims(2)+1)) .* floor(nsteps * y/(dims(1)+1)) .* floor(nsteps * z/(dims(3)+1));
    u = normalize(u,0,1);
elseif strcmpi(type,'beads')
    if nargin < 3
        nbeads = 5;
    else
        nbeads = varargin{1};
    end
    if nargin < 4
        r = 3;
    else
        r = varargin{2};
    end
    u = zeros(dims);
    p = round(dims / nbeads);
    if dims(3) == 1
        u(mod((1:dims(1))+round(p(1)/2),p(1))==0, mod((1:dims(2))+round(p(2)/2),p(2))==0) = 1;
    else
        u(mod((1:dims(1))+round(p(1)/2),p(1))==0, mod((1:dims(2))+round(p(2)/2),p(2))==0, mod((1:dims(3))+round(p(3)/2),p(3))==0) = 1;
    end
    u = imdilate(u, strel('sphere',r));
    u = normalize(u,0,1);
elseif strcmpi(type, 'tissue')
    u = zeros(dims);
    n = 30; 
    p = round(1 + repmat((dims(:) - 1),1,n) .* rand(numel(dims),n));
    for i = 1:n 
        u(p(1,i),p(2,i),p(3,i)) = 1;
    end
    D = bwdist(u,'euclidean');
    if ndims(D)==3
        K = {[0 1 0;1 1 1;0 1 0],[1,1,1],[1;1;1],[1,0,0;0,1,0;0,0,1], [0,0,1;0,1,0;1,0,0],cat(3,[0,0,0;0,1,0;0,0,0], [0,0,0;0,1,0;0,0,0], [0,0,0;0,1,0;0,0,0])};
    else
        K = {[1,1,1],[1;1;1], [1,0,0;0,1,0;0,0,1], [0,0,1;0,1,0;1,0,0]};
    end
    u = false(dims);
    for i = 1:numel(K)
        u = u | (D == imdilate(D, K{i}));
    end
    u = double(u);
    u(1,:,:) = 0;
    u(end,:,:) = 0;
    u(:,1,:) = 0;
    u(:,end,:) = 0;
    if ndims(u) == 3
        %u(:,:,1) = 0;
        %u(:,:,end) = 0;
        u = smooth3(u,'gaussian');
    else
        u = imfilter(u,fspecial('gaussian',3,1));
    end
    u = normalize(u,0,1);
elseif strcmpi(type, '1/f noise')
    u = randn(dims);
    fx = [0:floor(dims(2)/2)-1, floor(-dims(2)/2):-1];
    fy = [0:floor(dims(1)/2)-1, floor(-dims(1)/2):-1];
    if numel(dims) == 3 && dims(3)~=1
        fz = [0:floor(dims(3)/2)-1, floor(-dims(3)/2):-1];
        [x,y,z] = meshgrid(fx,fy,fz);
        D = sqrt(x.*x + y.*y + z.*z);
    else
        [x,y] = meshgrid(fx,fy);
        D = sqrt(x.*x + y.*y);
    end
    u = real(ifftn( u ./ (1+D).^2));
    u = normalize(u,0,1);
elseif  strcmpi(type, 'random spots')
    dims
    u = zeros(dims);
    n = 50;
    if numel(dims)==3 && dims(3) ~= 1
        [x,y,z] = meshgrid(1:dims(2),1:dims(1),1:dims(3));
        for k = 1:n
            p = dims(:) .* rand(1,numel(dims));
            u = u + exp(-0.5* ( (x-p(2)).^2 + (y-p(1)).^2 + (z-p(3)).^2 ) );
        end
    else
        [x,y] = meshgrid(1:dims(2),1:dims(1));
        for k = 1:n
            p = dims(:) .* rand(numel(dims),1);
            u = u + exp(-0.5* ( (x-p(2)).^2 + (y-p(1)).^2 ) );
        end
    end
    u = normalize(u,0,1);
elseif strcmpi(type,'cameraman')
    u = im2double(imread('https://homepages.cae.wisc.edu/~ece533/images/cameraman.tif'));
    u = imresize(u,dims(1:2));
else
    error('underfined type (%s)', type);
end

function y = normalize(x,a,b)
m = min(x(:));
M = max(x(:));
y = (x - m) / (M - m) *( b - a) + a;

function [kx,ky,kz] = freqgrid(dims)
fx = [0:floor(dims(2)/2)-1, floor(-dims(2)/2):-1] / (dims(2));
fy = [0:floor(dims(1)/2)-1, floor(-dims(1)/2):-1] / (dims(1));
fz = [0:floor(dims(3)/2)-1, floor(-dims(3)/2):-1] / (dims(3));
[kx,ky,kz] = meshgrid(fx,fy,fz);
