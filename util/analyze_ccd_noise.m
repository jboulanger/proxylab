function [p,S] = analyze_ccd_noise(img, doplot)
%
%  [p, S] = analyze_ccd_noise(img, doplot)
%
% Estimate CCD noise parameters from image
%   Input:
%       img : 2D/3D image
%   Ouput:
%       p parameters of noise as "Var[X] = p(2) + p(1) E[X]"
%       S local variance so that X' S^-1 X is a data term.
%   Example:
%      analyze_ccd_noise(poissrnd(ones(1,512)'*(1:512)))
%
%   Jerome Boulanger (2013) jerome.boulanger@curie.fr

if nargin < 2
    doplot = false;
end
  d = 5;
  if ismatrix(img)
      % compute pseudo-residuals
      % (normalized so that std(R) = std(img) if img is white noise)
      % del2=delta/2/n
      R =  del2(img) / 1.12;
      % Use a median filter as a robust estimate of the mean
      EX = medfilt2(img, [d d], 'symmetric');
      % Use the local Median of Absolute Filter for the variance
      VarX = (1.4826 * medfilt2(abs(R-medfilt2(R, [d d], 'symmetric')), [d d], 'symmetric')).^2;
      % Remove sides of the images
      EXb = EX(d : size(EX,1) - d, d : size(EX,2) - d);
      VarXb = VarX(d : size(VarX, 1) - d, d : size(VarX, 2) - d);
  else
      % compute pseudo-residuals
      % (normalized so that std(R) = std(img) if img is white noise)
      R = del2(img) / 1.087;
      % Use a median filter as a robust estimate of the mean
      EX = medfilt3(img, [d d d], 'symmetric');
      % Use the local Median of Absolute Filter for the variance
      VarX = (1.4826 * medfilt3(abs(R-medfilt3(R, [d d d], 'symmetric')), [d d d], 'symmetric')).^2;
      % Remove sides of the images
      EXb = EX(d : size(EX,1) - d, d : size(EX,2) - d, d:size(EX,3)-d);
      VarXb = VarX(d : size(VarX, 1) - d, d : size(VarX, 2) - d, d:size(VarX,3)-d);
  end
  % Regression of EX versus VarX
  %p = polyfit(EXb, VarXb, 1);
  p = robustfit(EXb(:), VarXb(:)); p = p([2,1]);
  
  if doplot
    accdshow(EX, VarX, p);
  end
  if nargout >= 2
     S = max(p(2) + p(1) .* EX, 1);
  end
end

function accdshow(R, R2, p)
% Show the regression in the mean-variance plane
%figure;
plot(R(:), R2(:), '.')
hold on
x = (min(R(:)) : max(R(:)));
plot(x, p(2) + p(1) * x,'r');
title(sprintf('CCD Noise Analysis \n VarX=%.2g+%.2gEX', p(2), p(1)));
xlabel('E[X]');
ylabel('Var[X]');
axis tight
hold off
R2s = sort(R2(:));
%sigma = sqrt(mean(R2(1:ceil(numel(R2)*0.001))));
sigma = sqrt(min(R2(:)));
fprintf(' gain: %f ADU/e- \n offset: %fADU \n readout: %fADU / %fe-\n',...
    p(1),min(R(:)), sigma, sigma/p(1));
a = min(R(:));
b = max(R(:));
fprintf(' dynamic range %f-%fADU/ %f photons\n', a, b, (b-a)/p(1));
end

