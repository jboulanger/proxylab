function data = generate_sim_data(obj,m,otf,zoom,gain,sigma,offset)
% [data,otf] = generate_sim_data(obj,m,otf,zoom,gain,sigma,m)
%
% Input:
%  obj : object / input image
%  m   : modulations as a 3D array 
%  otf : optical transfert function centered in (1,1)
%  zoom : zoom factor (either 2=downsampling by 2, or 1)
%  gain : sensor gain
%  sigma : standard deviation of the readout noise
%  offset : base level of the sensor
%
% Output:
%  data : collection of the modulated blurred downsampled images
% 
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)
L = size(m,3);
data = real(ifft2(fft2(m.*repmat(obj,[1 1 L])) .* repmat(otf,[1 1 L])));
if zoom == 2
    % downsample data;
    data = fftshift(fftshift(fft2(data),1),2);
    data = data(size(data,1)/4+1:3*size(data,1)/4,size(data,2)/4+1:3*size(data,2)/4,:);
    data = real(ifft2(ifftshift(ifftshift(data,2),1)));    
end

% add noise
data =  gain * double(imnoise(uint16(max(0,data-offset)/gain), 'poisson')) + sigma * randn(size(data)) + offset;
