function img = append_tiles(data,N,M)
% data = append_tiles(data,N,M)
%
% create a 2D array from a 3D array
%
% Jerome Boulanger (2015)

width = size(data,2);
height = size(data,1);
img = zeros(N * width, M * height);
n = 1;
for k = 1:N
    for l = 1:M
        if n <= size(data, 3)
            x0 = (k - 1) * width + 1;
            x1 = k * width;
            y0 = (l - 1) * height + 1;
            y1 = l * height;
            img(y0:y1, x0:x1) = data(:,:,n);
            n = n + 1;
        end
    end
end

end
