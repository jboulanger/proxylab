function [coefficients, sigma] = fit_psf_zernike(im,px,NA,wavelength,n,coefficients0,sigma0)
% coefficients = fit_psf_zernike(im,px,NA,wavelength,n,sigma,coefficients)
% 
% Estimate the zernike coefficient of the pupille function using lsqnonlon
%
% Input: 
%   im: the image
%   px: pixel size as an array [dy dx dz]
%   NA: Numerical aperture
%   wavelength : wavelength
%   n : refractive index
%   sigma: object size
%   coefficients0 : initialization. The function fits the number of
%   coefficients defined by this vector.
%
% Output:
%  coefficients : estimated coefficients
%  sigma : the gaussian std
%
% needs the function zernike_poly

dims = size(im);
% define the frequency space in x,y,z and direct space in z
fx = [0:floor(dims(2)/2)-1, floor(-dims(2)/2):-1] / (dims(2)*px(2));
fy = [0:floor(dims(1)/2)-1, floor(-dims(1)/2):-1] / (dims(1)*px(1));
fz = [0:floor(dims(3)/2)-1, floor(-dims(3)/2):-1] / (dims(3)*px(3));
ez = [0:floor(dims(3)/2)-1, floor(-dims(3)/2):-1] * px(3);
[kx,ky,z] = meshgrid(fx,fy,ez);
[~,~,kz] = meshgrid(fx,fy,fz);
rho = sqrt(kx.^2 + ky.^2) * wavelength / NA;
theta = atan2(ky,kx);

% normalize the image between 0 and 1
im = (im - min(im(:))) / (max(im(:)) - min(im(:)));
% initial estimate with intensity scaling
x0 = [1; 0; sigma0/1000; coefficients0(:)];
% uppper and lower bounds
lb = [0.5; -0.5; 0.5; -3*ones(numel(coefficients0),1)];
ub = [1.5; 0.5; 5;3*ones(numel(coefficients0),1)];
xest = lsqnonlin(@(x) cost(x,im,kx,ky,kz,z,rho,theta,wavelength,n), x0, lb, ub);
coefficients = xest(4:end);
sigma = xest(3) * 1000;

function residuals = cost(coefficients,im,kx,ky,kz,z,rho,theta,wavelength,n)
% compute the psf as the square of the inverse Fourier transform of the
% pupil function (autocorrelation of the pupil function)
obj = exp(-0.5*(kx.^2 + ky.^2 + kz.^2) * (coefficients(3)*1000).^2); % gaussian object
defocus = z .* sqrt( max(0, (n/wavelength).^2 - kx.^2 - ky.^2 ));
phi = zernike_poly(rho, theta, coefficients(4:end));
psf = abs(fft2((rho<1) .* exp(-2*pi*1i* (defocus + phi)))).^2;
otf = fftn(psf);
otf = otf ./ otf(1,1,1);
%otf(abs(otf)<1e-6) = 0;
bead = fftshift(real(ifftn(otf.*obj)));
bead = (bead - min(bead(:))) / (max(bead(:)) - min(bead(:)));
bead = (bead - coefficients(2)) / coefficients(1);
residuals = im - bead;
J = mean((abs(residuals(:)).^2));

subplot(231), imshow(fftshift(obj(:,:,1)),[]); title('Obj')
subplot(232), imshow(fftshift(phi(:,:,1)),[]); title('Phase')
subplot(233), bar(coefficients); title('Coefficients');
subplot(234), imshow3(sqrt(im),[],'mip');  title('Data')
subplot(235); imshow3(sqrt(max(0,bead)),[],'mip'); title('Simulation')
subplot(236), imshow3(abs(residuals),[],'mip'); title(sprintf('|Error| %.5f',sqrt(J)));
drawnow;