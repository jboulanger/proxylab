function fftshow(data,otf)
% fftshow(data,otf)
%
% display the log magnitude of the fourier transform
%
% Input
%  data : array (2D or 3D)
%  otf  : optical transfert function use ot draw the cutoff frequency
% 
% Jerome Boulanger 2018
%
    if ismatrix(data)
        imshow(fftshift(log(1+abs(fftn(data)))),[])
    else
        imshow3(fftshift(log(1+abs(fftn(data)))),[],'slice')
    end
    if nargin > 1           
        hold on;
        r0 = find(otf(1,:)<eps,1);
        t = linspace(0,2*pi);
        plot(size(data,2)/2+r0*cos(t),size(data,1)/2+r0*sin(t));
        hold off
    end
end