function cost = create_positivity_cost(lambda)
%
% cost = create_positivity_cost()
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 1
    lambda = 1;
end

cost = create_cost_term(create_positivity_fun(lambda));

