function dual = eval_dual_cost(cost,y,bounds,Lsyi)
% Compute the primal dual gap for a cost function of the form:
% G(x) + sum_i H_i(L_i(x))
% where G is the indicator function of the set determined by "bounds":
% bounds(1) \leq x_j \leq bounds(2), \forall j
%
% y: dual variable
% bounds: [bounds(1),bounds(2)], lower and upper bounds for entries of x
% Lsyi: L_i*(y_i), if calculated outside this function
%
% Output:
% dual   : dual cost

if nargin < 5
    for i=1:length(cost)
        Lsyi{i} = cost(i).operator.apply_adjoint(y{i});
    end
end

% Dual
Lsy = 0;
Hsy = 0;
for i = 1:length(cost)
    Lsy = Lsy + Lsyi{i};
    Hsy = Hsy + cost(i).function.conv_conj(y{i});
end

Gs = indicator_convex_conjugate(-Lsy,bounds);

dual = - Gs - Hsy;

% Then, the primal-dual gap is sum(primal)-dual
end

