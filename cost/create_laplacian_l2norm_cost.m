function cost = create_laplacian_l2norm_cost(lambda)
%
% cost = create_laplacian_l2norm_cost(lambda)
%
% Create a cost term |Lx|_2 with L the Laplacian operator
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

h = fspecial('laplacian');
h = h / sqrt(2*sum(h(:).^2));
cost = create_cost_term(create_l2norm_fun(lambda), create_convolution_op(h));
cost.operator.name = 'Laplacian';
cost.operator.norm = 1;
