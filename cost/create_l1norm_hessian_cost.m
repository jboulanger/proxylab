function cost = create_l1norm_hessian_cost(lambda, D)
%
% cost = create_hessian_entropy_cost(lambda)
%
% Create a cost term entropy(Hx) with H the Hessian operators
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)


% create a filter bank for the hessian 
if D == 2
    a = 5.6527;
    H{1} = [1 -2 1] / a;
    H{2} = [1 0 -1; 0 0 0; -1 0 1] / (2*a);
    H{3} = [1 0 -1; 0 0 0; -1 0 1] / (2*a);
    H{4} = [1; -2; 1] / a;
else
    a = 6.9118;
    f = [1 -2 1] / a; % term
    g =  [1 0 -1; 0 0 0; -1 0 1] / (2*a); % cross term
    H{1} = f;                   % Ixx
    H{2} = g;                   % Ixy
    H{3} = reshape(g, [3 1 3]); % Ixz
    H{4} = H{2};                % Ixy
    H{5} = f';                  % Iyy
    H{6} = reshape(g, [1 3 3]); % Iyz
    H{7} =  H{3};               % Ixz
    H{8} = H{6};                % Iyz
    H{9} = reshape(f, [1 1 3]); % Izz
end


cost = create_cost_term(create_l1norm_fun(lambda), create_filter_bank_op(H));
cost.operator.name = 'Hessian';