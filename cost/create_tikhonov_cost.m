function cost = create_tikhonov_cost(lambda, D)
%
% cost = create_tikhonov_cost(lambda)
%
% Create a cost term |Dx|_2 with D the gradient operators
%
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 2
  D = 2;
end

if D == 2
  gradients{1} = [0,-1,1] /sqrt(8);
  gradients{2} = [0,-1,1]'/sqrt(8);
  opnorm = 1;
else
  gradients{1} = [0,-1,1];
  gradients{2} = [0,-1,1]';
  gradients{3} = reshape(gradients{1},[1,1,3]);
  opnorm = 6;
end

cost = create_cost_term(create_l2norm_fun(lambda), create_filter_bank_op(gradients));
cost.operator.name = 'grad';
cost.operator.norm = opnorm;
%compute_operator_norm(cost.operator, randn(128,128),500)