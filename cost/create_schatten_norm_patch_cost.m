function cost = create_schatten_norm_patch_cost(lambda, patch, window)
%
% cost = create_patch_schatten_norm_cost(lambda, patch, window)
%
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

cost = create_cost_term(create_schatten_norm_fun(lambda), create_patch_dictionary_op(patch, window));

