function cost = create_total_variation_cost(lambda, D)
%
% cost = create_total_variation_cost(lambda)
%
% Create a cost term |Dx|_1 with D the gradient operators
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

if nargin < 2
    D = 2;
end

% a ensures that the operator norm is 1
if D == 2
    a = sqrt(8);
    gradients{1} = [0, -1, 1]  / a;
    gradients{2} = [0, -1, 1]' / a;    
elseif D == 3    
    a = 3.3933;
    gradients{1} = [0, -1, 1] / a;
    gradients{2} = [0, -1, 1]' / a;
    gradients{3} = reshape(gradients{1},[1,1,3]);
end

cost = create_cost_term(create_l1norm_fun(lambda,[], true), create_filter_bank_op(gradients));
cost.operator.name = 'grad';
cost.operator.norm = 1;
