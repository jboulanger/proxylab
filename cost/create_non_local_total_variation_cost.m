function cost = create_non_local_total_variation_cost(lambda, img, patch, window)
%
% cost = create_non_local_total_variation_cost(lambda)
%
% Create a cost term W|Dx|_1 with D the gradient operators
%
% Input:
%  lambda : multiplier
%  img    : image
%  patch  : patch size
%  window : window size
%  sigma : tonal bandwidth
%
% Output:
%   cost
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

cost = create_cost_term(create_l1norm_fun(lambda), create_non_local_op(img, patch, window));
cost.operator.name = 'non-local';
