function c = eval_cost(cost, x)
%
% c = eval_cost_term(cost_term, x)
%
% Evaluate the cost_term at point x
%
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)

for i = 1:numel(cost)
    c(i) = cost(i).function.eval(cost(i).operator.apply(x));
end