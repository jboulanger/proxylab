function opnorm = compute_multi_operator_norm(cost,x0,max_iter)
%
%   L = compute_operator_norm(cost, x0, max_iter)
%   L = compute_operator_norm(cost, x0)
%
%  Compute the norm of the operator || sum_i (L* L) ||_2
%
% Input:
%   cost : struct array with field 'operator', having functions 'apply' and 'apply_adjoint'
%   x0       : data
%   max_iter : number of iterations (default:500)
%
% Output
%   L        : the norm of the operator
%
% Jerome Boulanger (jeromeb@mrc-lmb.cam.ac.uk)
%
% Idea: the operator being Hermitian, its norm is equal to the 
% maximum eigenvalue. The dominant eigenvector is calculated 
% the power method, then its corresponding eigenvalue is obtained
% using the Rayleigh quotient.

if nargin < 3
  max_iter = 500;
end

x1 = x0+1;
k = 1;
while norm(x1(:)-x0(:)) > eps && k < max_iter
  x0 = x1;
  x1 = zeros(size(x0));
  for q=1:numel(cost)
    x1 = x1 + cost(q).operator.apply_adjoint(cost(q).operator.apply(x0));
  end
  x1 = x1./norm(x1(:),2);
  k = k+1;
end

opnorm = 0;
for q=1:numel(cost)
  Ax = cost(q).operator.apply(x1);
  opnorm = opnorm + sum(Ax(:).^2);
end
opnorm = opnorm/(sum(x1(:).^2));

