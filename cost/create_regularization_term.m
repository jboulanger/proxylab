function cost = create_regularization_term(lambda, type, img)
%
% cost = create_regularization_term(lambda, type, img)
%  
% Create a regularization cost term
%
% This is a short hand to create a set of predefined regularization terms
%
% Input
%  lambda : the regularization parameter
%  type   :'tikhonov', 'l2-laplacian', 'l2-laplacian', 'tv', 'nltv', 
%           'schatten-hessian', 'schatten-patch'
if strcmpi(type,'tikhonov')
    cost = create_tikhonov_cost(lambda);
elseif strcmpi(type,'l2-laplacian')
    cost = create_laplacian_l2norm_cost(lambda);
elseif strcmpi(type,'l1-laplacian')
    cost = create_laplacian_l1norm_cost(lambda);
elseif strcmpi(type,'tv')
    cost = create_total_variation_cost(lambda, ndims(img));
elseif strcmpi(type,'nltv')
    cost = create_non_local_total_variation_cost(lambda, img, 4, 8);
elseif strcmpi(type,'schatten-hessian')
    cost = create_schatten_norm_hessian_cost(lambda, ndims(img));
elseif strcmpi(type,'entropy-hessian')
    cost = create_entropy_hessian_cost(lambda,ndims(img));
elseif strcmpi(type,'l1-hessian')
    cost = create_l1norm_hessian_cost(lambda,ndims(img));
elseif strcmpi(type,'schatten-patch')
    cost = create_schatten_norm_patch_cost(lambda, 8, 16);
else
  error('unknown regularization (%s)', type)
end
