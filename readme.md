PROXYLAB
========

Inverse problem reconstruction matlab/octave toolbox based on proximal method for the optimization of cost functions.

This code was developed in the frame of this work:

J. Boulanger, N. Pustelnik, L. Condat, L. Sengmanivong, and T. Piolot, ‘Nonsmooth convex optimization for structured illumination microscopy image reconstruction’, Inverse Problems, vol. 34, no. 9, p. 095004, 2018. https://iopscience.iop.org/article/10.1088/1361-6420/aaccca


# Introduction


Define a cost function composed of an array of cost terms:
``` math
J(x) = \sum_{q=1}^Q f_q(T_q)
```
A cost term is represented as a structure having the fields 'function' and 'operator' representing respectively fq and Tq.

The 'function' is itself a structure with the field 'eval', 'prox' or 'grad', the 'operator' has the fields 'apply', 'adjoint_apply' and 'norm'. Both function and operator can have a field 'name' to help trace the cost functions. The exact needed fields will depend on the optimizaiton algorithm used to minimize the cost function. This results is a nested structure of the type:

```  matlab
cost(1).function.eval = @(x) ...;
cost(1).function.prox = @(x) ...;
cost(1).prox = @(x) ...;
cost(1).operator.apply =  @(x) ...;
cost(1).operator.apply_adjoin =  @(x) ...;
cost(1).operator.norm  = ...;
```

Writing of cost function can be simplified by combining existing function and operators using the function create\_cost\_function, for example:
``` matlab
gradients{1} = [0, -1, 1];
gradients{2} = [0, -1, 1];
cost(1) = create_cost_function(create_l1_norm(1,[],true), ...
                               create_filter_bank_op(gradients));
cost(1).operator.name = 'grad';
```
or
``` matlab
 cost = create_total_variation_cost(lambda);
```
or
``` matlab
 cost = create_regularization_term(lambda, 'tv');
```

Cost function can be then minimized using various algorithms depending on the properties of the cost terms. The general syntax is:
``` matlab
output = pdhg(cost,options);
```

## Obervers
A callback can be added to the options in order to monitor the progress of the algorithm:
``` matlab
options.observer = @(cost,options,output)...;
```
Predefined observers are available in the folder 'observer'. For example, to monitor the iterations, we can use:
``` matlab
options.observer = @progressbar;
```
or to display the restored image along with the cost function:
``` matlab
options.observer = @observer0;
```

Structure of the repository
---------------------------

- algorithms: minimization algorithms
- cost: definition of usual cost terms
- function: functions f that can be used to define the cost
- observer: tools to monitor the evolution of the optimization
- util: utilities
- examples: examples demonstrating the use of the toolbox

Related softwares
-----------------
- http://proximity-operator.net
- https://epfl-lts2.github.io/unlocbox-html/doc/prox/
- https://github.com/Biomedical-Imaging-Group/GlobalBioIm
- https://www.cs.umd.edu/~tomg/projects/pdhg/
- https://sites.google.com/site/fomsolver/home
- https://arxiv.org/abs/1712.05602
